﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Core.Settings;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GreenshotPlugin.Modules {
	/// <summary>
	/// This is a static wrapper for the MEF container which can be used to access plugins etc
	/// </summary>
	public class ModuleContainer {
		/// <summary>
		/// Satisfy all the [Import]s in the supplied object
		/// The object is only injected, not registered or anything else.
		/// Can be used in components that are not exported, but still need imports
		/// </summary>
		/// <param name="thethis">object to inject to</param>
		public static void InjectThis(object thethis) {
			MefContainer.SatisfyImportsOnce(thethis);
		}

		/// <summary>
		/// Use this container to get any exported modules like this:
		/// var shutdownActions = from export in ModuleContainer.MefContainer.GetExports<IShutdownAction>() where export.Value.IsActive orderby export.Value.Priority descending select export;
		/// This Container can also be imported with [Import(typeof(CompositionContainer))]
		/// </summary>
		public static CompositionContainer MefContainer {
			get;
			set;
		}
	}

}
