﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Greenshot.IniFile;
using GreenshotPlugin.UnmanagedHelpers;
using GreenshotPlugin.Core;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using log4net;
using Screen = System.Windows.Forms.Screen;
using Region = System.Drawing.Region;
using System.Windows.Media;
using GreenshotPlugin.Modules;
using Point = System.Windows.Point;
using System.ComponentModel.Composition;

namespace GreenshotPlugin.Modules {
	/// <summary>
	/// This is the stack source, and makes it possible to stack multiple sources
	/// </summary>
	public class StackSource : ISource {
		public List<ISource> Sources {
			get;
			set;
		}

		public StackSource() {
			Sources = new List<ISource>();
		}

		public bool Import(CaptureContext captureContext) {
			bool returnValue = true;
			foreach (var source in Sources) {
				returnValue &= source.Import(captureContext);
			}
			return returnValue;
		}
	}
}
