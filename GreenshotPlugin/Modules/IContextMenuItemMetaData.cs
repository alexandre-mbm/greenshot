﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
namespace GreenshotPlugin.Modules {

	/// <summary>
	/// This is the attribute which can be used for the type-safe meta-data
	/// </summary>
	[MetadataAttribute]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class ContextMenuExecuteAttribute : ExportAttribute, IContextMenuItemExecuteMetadata {
		public ContextMenuExecuteAttribute() : base (typeof(IContextMenuExecuteItem)) {
		}
		public string HeaderLanguageKey {
			get;
			set;
		}
		/// <summary>
		/// Filename of the icon (SVG) to use
		/// </summary>
		public string IconFilename {
			get;
			set;
		}
		/// <summary>
		/// All ContextMenuItems are sorted by location, if the location changes a separator is added
		/// This still needs to be changed, as this prevents to have a real order in the "parts".
		/// </summary>
		public int Location {
			get;
			set;
		}
	}

	[MetadataAttribute]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class ContextMenuCanExecuteAttribute : ExportAttribute, IContextMenuItemCanExecuteMetadata {
		public ContextMenuCanExecuteAttribute() : base(typeof(IContextMenuCanExecuteItem)) {
		}
		public string HeaderLanguageKey {
			get;
			set;
		}
	}


	/// <summary>
	/// This is the interface for the MEF meta-data
	/// </summary>
	public interface IContextMenuItemExecuteMetadata {
		string HeaderLanguageKey {
			get;
		}
		[DefaultValue(null)]
		string IconFilename {
			get;
		}
		[DefaultValue(0)]
		int Location {
			get;
		}
	}

	/// <summary>
	/// This is the interface for the MEF meta-data
	/// </summary>
	public interface IContextMenuItemCanExecuteMetadata {
		string HeaderLanguageKey {
			get;
		}
	}

	/// <summary>
	/// Marker interface for the Context menu "execute" method, should be of type void Name(object commandParameter)
	/// </summary>
	public interface IContextMenuExecuteItem {
	}
	/// <summary>
	/// Marker interface for the Context menu "can execute", should be of type bool Name(object commandParameter)
	/// </summary>
	public interface IContextMenuCanExecuteItem {
	}
}
