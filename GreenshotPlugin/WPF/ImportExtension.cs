﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Markup;

namespace GreenshotPlugin.WPF {
	public class ImportExtension : MarkupExtension {
		public Type Contract {
			get;
			set;
		}
		public bool IsDesigntimeSupported {
			get;
			set;
		}

		public ImportExtension() {
		}

		public ImportExtension(Type contract)
			: this(contract, false) {
		}

		public ImportExtension(Type contract, bool isDesigntimeSupported) {
			Contract = contract;
			IsDesigntimeSupported = isDesigntimeSupported;
		}

		public override object ProvideValue(IServiceProvider serviceProvider) {
			if (Contract == null) {
				throw new ArgumentException("Contract must be set with the contract type");
			}

			var service = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
			if (service == null) {
				throw new ArgumentException("IProvideValueTarget service is missing");
			}

			var target = service.TargetObject as DependencyObject;
			if (target == null) {
				// TODO : Handle DataTemplate/ControlTemplate case...
				throw new ArgumentException("The target object of ImportExtension markup extension must be a dependency object");
			}

			var property = service.TargetProperty as DependencyProperty;
			if (property == null) {
				throw new ArgumentException("The target property of ImportExtension markup extension must be a dependency property");
			}

			object value;
			if (DesignerProperties.GetIsInDesignMode(target)) {
				value = ImportDesigntimeContract(target, property);
			} else {
				value = ImportRuntimeContract(target, property);
			}

			return value;
		}

		private object ImportDesigntimeContract(DependencyObject target, DependencyProperty property) {
			if (IsDesigntimeSupported) {
				return ImportRuntimeContract(target, property);
			}

			return DependencyProperty.UnsetValue;
		}

		private object ImportRuntimeContract(DependencyObject target, DependencyProperty property) {
			var bootstrapper = CompositionProperties.GetBootstrapper(Application.Current);
			if (bootstrapper == null) {
				throw new InvalidOperationException("Composition bootstrapper was not found. You should attach a CompositionBootstrapper with the Application instance.");
			}

			return GetExportedValue(bootstrapper.Container);
		}

		private object GetExportedValue(CompositionContainer container) {
			var exports = container.GetExports(Contract, null, null).ToArray();
			if (exports.Length == 0) {
				throw new InvalidOperationException(string.Format("Couldn't resolve export with contract of type {0}. Please make sure that the assembly contains this type is loaded to composition.", Contract));
			}

			var lazy = exports.First();
			return lazy.Value;
		}
	}
}
