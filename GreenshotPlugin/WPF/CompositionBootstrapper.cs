/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// Code comes from http://www.codeproject.com/Articles/155802/Blendability-Part-IV-Design-Time-Support-for-MEF
	/// </summary>
	public abstract class CompositionBootstrapper {
		protected AggregateCatalog AggregateCatalog {
			get;
			private set;
		}

		public CompositionContainer Container {
			get;
			private set;
		}

		protected CompositionBootstrapper() {
			AggregateCatalog = new AggregateCatalog();
		}

		/// <summary>
		/// Override this method to extend what is loaded into the Catalog
		/// </summary>
		protected virtual void ConfigureAggregateCatalog() {
		}

		protected virtual void ConfigureContainer() {
			// Export the container itself
			Container.ComposeExportedValue<CompositionContainer>(Container);
		}

		public void Run() {
			ConfigureAggregateCatalog();
			Container = new CompositionContainer(AggregateCatalog);
			ConfigureContainer();
			Container.ComposeParts();
		}
	}
}
