﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using GreenshotPlugin.Core.Settings;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// The LanguageLabel allows you show a translation in a label, where binding is possible
	/// </summary>
	public class LanguageLabel : Label {
		public static readonly DependencyProperty LanguagePrefixProperty = DependencyProperty.Register("LanguagePrefix", typeof(string), typeof(LanguageLabel));
		public static readonly DependencyProperty LanguageKeyProperty = DependencyProperty.Register("LanguageKey", typeof(string), typeof(LanguageLabel));

		/// <summary>
		/// The LanguagePrefix-property to bind to
		/// </summary>
		public string LanguagePrefix {
			get {
				return (string)GetValue(LanguagePrefixProperty);
			}
			set {
				SetValue(LanguagePrefixProperty, value);
			}
		}

		/// <summary>
		/// The language-key for the translation
		/// </summary>
		public string LanguageKey {
			get {
				return (string)GetValue(LanguageKeyProperty);
			}
			set {
				SetValue(LanguageKeyProperty, value);
			}
		}

		protected override void OnInitialized(EventArgs e) {
			base.OnInitialized(e);

			this.ApplySettingsStyle();

			if (this.IsDesignMode()) {
				Content = LanguageKey;
				return;
			}

			this.Translate(LanguagePrefix, LanguageKey);
		}
	}
}
