﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// The DictionaryItemConverter can be used for Multi-Bindings, when one has a variable with the dictionary key
	/// 
	/// Example can be found in the DestinationPickerWindow.xaml:
	/// 
	/// <MultiBinding Converter="{StaticResource DictionaryItemConverter}">
	///		<Binding ElementName="BaseWindow" Path="DataContext.SvgImages"/>
	///		<Binding Path="Metadata.IconFilename" />
	/// </MultiBinding>
	/// </summary>
	public class DictionaryItemConverter : IMultiValueConverter {
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
			if (values != null && values.Length >= 2) {
				var dictionary = values[0] as dynamic;
				var key = values[1] as string;
				if (dictionary != null && key != null) {
					if (dictionary.ContainsKey(key)) {
						return dictionary[key];
					}
				}
			}
			return Binding.DoNothing;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
			throw new NotSupportedException();
		}
	}
}
