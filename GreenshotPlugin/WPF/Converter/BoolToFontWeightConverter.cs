﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Windows;
using System.Windows.Data;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// This converter takes a bool and converts it to a FontWeight
	/// The default is:
	///		true -> FontWeights.Bold
	///		false -> FontWeights.Normal
	///	But by setting the True and False values this can be changed.
	/// </summary>
	[ValueConversion(typeof(bool), typeof(FontWeight))]
	public class BoolToFontWeightConverter : IValueConverter {
		public FontWeight TrueValue {
			get;
			set;
		}
		public FontWeight FalseValue {
			get;
			set;
		}

		public BoolToFontWeightConverter() {
			// set defaults
			TrueValue = FontWeights.Bold;
			FalseValue = FontWeights.Normal;
		}
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			if (value != null) {
				if ((bool)value) {
					return TrueValue;
				}
				return FalseValue;
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (FontWeight)value == TrueValue;
		}
	}
}
