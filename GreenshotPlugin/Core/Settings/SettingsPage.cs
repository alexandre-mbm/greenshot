﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin.Core;
using System.Windows.Controls;
using Greenshot.IniFile;
using System.Windows;
using System;
using System.IO;
using System.Windows.Markup;
using System.Windows.Resources;
using System.Reflection;
using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using System.Collections.Generic;

namespace GreenshotPlugin.Core.Settings {
	/// <summary>
	/// This is the attribute which can be used for the type-safe meta-data
	/// </summary>
	[MetadataAttribute]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public class SettingsPageAttribute : ExportAttribute, ISettingsPageMetadata {
		public SettingsPageAttribute() : base(typeof (SettingsPage)) {
		}
		public string Path {
			get;
			set;
		}
	}
	/// <summary>
	/// This is the interface for the MEF meta-data
	/// </summary>
	public interface ISettingsPageMetadata {
		string Path {
			get;
		}
	}
	/// <summary>
	/// Base class for all the settings pages
	/// </summary>
	public abstract class SettingsPage : Page, IPartImportsSatisfiedNotification {
		[Import]
		public IDictionary<string, IniProxy> IniSections {
			get;
			set;
		}

		/// <summary>
		/// Override to initialize your page!
		/// </summary>
		protected virtual void Initialize() {
		}

		public SettingsPage() : base() {
			DataContext = this;
		}


		/// <summary>
		/// The Cancel method rolls-back the changes to the proxy
		/// </summary>
		public virtual void Rollback() {
		}

		/// <summary>
		/// The Cancel method rolls-back the changes to the proxy
		/// </summary>
		public virtual void Commit() {
		}

		/// <summary>
		/// Is called from the Composition constuctor, when all imports are satisfied
		/// </summary>
		public void OnImportsSatisfied() {
			Initialize();
		}
	}
}
