﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.ComponentModel;
using GreenshotPlugin.WPF;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace GreenshotPlugin.Core.Settings {
	public static class SettingsExtensions {
		/// <summary>
		/// Is the control is design mode?
		/// </summary>
		/// <param name="contol"></param>
		/// <returns>true</returns>
		public static bool IsDesignMode(this Control contol) {
			return DesignerProperties.GetIsInDesignMode(contol);
		}

		/// <summary>
		/// Set the content to the translated value
		/// </summary>
		/// <param name="control"></param>
		/// <param name="languageKey">Key to use for the translation</param>
		public static void Translate(this ContentControl control, string prefix, string languageKey) {
			if (control.Content == null && languageKey != null) {
				control.Content = Language.GetString(prefix, languageKey);
			}
		}


		/// <summary>
		/// Check if the control's prop has a binding
		/// </summary>
		/// <param name="control"></param>
		/// <param name="prop"></param>
		/// <returns></returns>
		public static bool HasBinding(this Control control, DependencyProperty prop) {
			return control.GetBindingExpression(prop) != null;
		}
	
		/// <summary>
		/// Apply a binding on the DependencyProperty "prop" on control if the current binding is null
		/// </summary>
		/// <param name="control"></param>
		/// <param name="prop"></param>
		/// <param name="iniSection"></param>
		/// <param name="iniProperty"></param>
		public static void SetIniBindingIfNull(this Control control, DependencyProperty prop, string iniSection, string iniProperty) {
			if (!control.HasBinding(prop)) {
				string path = string.Format("IniSections[{0}][{1}].Value", iniSection, iniProperty);
				Binding binding = new Binding(path);
				control.SetBinding(prop, binding);
			}
		}

		/// <summary>
		/// Apply the settings Style to the control
		/// </summary>
		/// <param name="control"></param>
		public static void ApplySettingsStyle(this Control control) {
			if (control.Style == null) {
				Style settingsStyle = (Style)Application.Current.TryFindResource("SettingsControl");
				if (settingsStyle != null) {
					control.Style = settingsStyle;
				}
			}
		}

		/// <summary>
		/// Apply a binding for the expert settings to the control
		/// </summary>
		/// <param name="control">Control to bind the VisibilityProperty to</param>
		/// <param name="iniSection">Config path, e.g CoreConfig or Config</param>
		/// <param name="iniProperty">Property in the config</param>
		public static void ApplyExpertSettingsBinding(this Control control, string iniSection, string iniProperty) {
			string path = string.Format("IniSections[{0}][{1}]", iniSection, iniProperty);
			if (!control.HasBinding(UIElement.VisibilityProperty)) {
				MultiBinding multiBinding = new MultiBinding();
				multiBinding.Converter = new OrBooleanConverter();

				Binding propertyVisibleBinding = new Binding(path + ".IsVisible");

				propertyVisibleBinding.Converter = new BooleanToVisibilityConverter();
				multiBinding.Bindings.Add(propertyVisibleBinding);
				Binding showExpertBinding = new Binding("IniSections[Core][ShowExpertSettings].Value");
				showExpertBinding.Converter = new BooleanToVisibilityConverter();
				multiBinding.Bindings.Add(showExpertBinding);
				control.SetBinding(UIElement.VisibilityProperty, multiBinding);
			}
			if (!control.HasBinding(Control.FontWeightProperty)) {
				Binding propertyExpertBinding = new Binding(path + ".IsExpert");
				propertyExpertBinding.Converter = new BoolToFontWeightConverter();
				control.SetBinding(Control.FontWeightProperty, propertyExpertBinding);
			}
		}

		/// <summary>
		/// Apply a binding for the fixed settings to the control
		/// </summary>
		/// <param name="control">Control to bind the IsEnabled to</param>
		/// <param name="iniSection">Config path, e.g CoreConfig or Config</param>
		/// <param name="iniProperty">Property in the config</param>
		/// <param name="dependsOnIsChecked">This control is only enabled if the dependsOnIsChecked element is checked</param>
		public static void ApplyFixedBinding(this Control control, string iniSection, string iniProperty, string dependsOnIsChecked) {
			string path = string.Format("IniSections[{0}][{1}]", iniSection, iniProperty);
			if (!control.HasBinding(UIElement.IsEnabledProperty)) {
				Binding isEnabledBinding = new Binding(path + ".IsEditable");
				if (dependsOnIsChecked == null) {
					control.SetBinding(UIElement.IsEnabledProperty, isEnabledBinding);
				} else {
					MultiBinding multiBinding = new MultiBinding();
					multiBinding.Converter = new AndBooleanConverter();
					multiBinding.Bindings.Add(isEnabledBinding);
					Binding dependsOnBinding = new Binding("IsChecked");
					dependsOnBinding.ElementName = dependsOnIsChecked;
					multiBinding.Bindings.Add(dependsOnBinding);
					control.SetBinding(UIElement.IsEnabledProperty, multiBinding);
				}
			}
		}
	}
}
