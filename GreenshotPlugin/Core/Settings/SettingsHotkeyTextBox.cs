﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;
using log4net;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using GreenshotPlugin.Modules;
using System.Windows.Media;
using Greenshot.IniFile;
using System.Windows.Controls;

namespace GreenshotPlugin.Core.Settings {
	/// <summary>
	/// A simple control that allows the user to select pretty much any valid hotkey combination
	/// See: http://www.codeproject.com/KB/buttons/hotkeycontrol.aspx
	/// But is modified to work with WPF and in Greenshot, also added localized support
	/// </summary>
	public class SettingsHotkeyTextBox : TextBox {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(SettingsHotkeyTextBox));
		public static readonly DependencyProperty ConfigPropertyProperty = DependencyProperty.Register("ConfigProperty", typeof(string), typeof(SettingsHotkeyTextBox));
		public static readonly DependencyProperty ConfigPathProperty = DependencyProperty.Register("ConfigPath", typeof(string), typeof(SettingsHotkeyTextBox));
		public static readonly DependencyProperty DependsOnIsCheckedProperty = DependencyProperty.Register("DependsOnIsChecked", typeof(string), typeof(SettingsHotkeyTextBox));

		/// <summary>
		/// The config-property to bind to
		/// </summary>
		public string ConfigProperty {
			get {
				return (string)GetValue(ConfigPropertyProperty);
			}
			set {
				SetValue(ConfigPropertyProperty, value);
			}
		}

		/// <summary>
		/// The path to the configuration object
		/// </summary>
		public string ConfigPath {
			get {
				return (string)GetValue(ConfigPathProperty);
			}
			set {
				SetValue(ConfigPathProperty, value);
			}
		}

		/// <summary>
		/// If the element which is named here has it's IsChecked checked, this element is enabled.
		/// </summary>
		public string DependsOnIsChecked {
			get {
				return (string)GetValue(DependsOnIsCheckedProperty);
			}
			set {
				SetValue(DependsOnIsCheckedProperty, value);
			}
		}

		public SettingsHotkeyTextBox() {
			// Handle events that occurs when keys are pressed
			PreviewKeyDown += ShortcutTextBox_PreviewKeyDown;
			PreviewKeyUp += ShortcutTextBox_PreviewKeyDown;
			Loaded += SettingsHotkeyTextBox_Loaded;
			SetValue(ConfigPathProperty, "Core");
		}

		protected override void OnInitialized(EventArgs e) {
			var parent = VisualTreeHelper.GetParent(this);
			while (!(parent is SettingsPage)) {
				parent = VisualTreeHelper.GetParent(parent);
			}
			settingsPage = parent as SettingsPage;
			base.OnInitialized(e);
			this.ApplySettingsStyle();

			if (this.IsDesignMode()) {
				return;
			}

			if (ConfigProperty != null) {
				if (this.HasBinding(TextProperty)) {
					BindingOperations.ClearBinding(this, TextProperty);
				}
			}
		}

		#region native keymap code
		[DllImport("user32.dll", EntryPoint = "GetKeyNameTextA", SetLastError = true)]
		private static extern int GetKeyNameText(uint lParam, [Out] StringBuilder lpString, int nSize);
		[DllImport("user32.dll", SetLastError = true)]
		private static extern uint MapVirtualKey(uint uCode, uint uMapType);

		private enum MapType : uint {
			MAPVK_VK_TO_VSC = 0, //The uCode parameter is a virtual-key code and is translated into a scan code. If it is a virtual-key code that does not distinguish between left- and right-hand keys, the left-hand scan code is returned. If there is no translation, the function returns 0.
			MAPVK_VSC_TO_VK = 1, //The uCode parameter is a scan code and is translated into a virtual-key code that does not distinguish between left- and right-hand keys. If there is no translation, the function returns 0.
			MAPVK_VK_TO_CHAR = 2,	  //The uCode parameter is a virtual-key code and is translated into an unshifted character value in the low order word of the return value. Dead keys (diacritics) are indicated by setting the top bit of the return value. If there is no translation, the function returns 0.
			MAPVK_VSC_TO_VK_EX = 3,	//The uCode parameter is a scan code and is translated into a virtual-key code that distinguishes between left- and right-hand keys. If there is no translation, the function returns 0.
			MAPVK_VK_TO_VSC_EX = 4 //The uCode parameter is a virtual-key code and is translated into a scan code. If it is a virtual-key code that does not distinguish between left- and right-hand keys, the left-hand scan code is returned. If the scan code is an extended scan code, the high byte of the uCode value can contain either 0xe0 or 0xe1 to specify the extended scan code. If there is no translation, the function returns 0.
		}
		#endregion

		private SettingsPage settingsPage = null;
		private IniValue _hotkeyValue = null;

		/// <summary>
		/// Key for the Hotkey
		/// </summary>
		public Key Hotkey {
			get;
			set;
		}

		/// <summary>
		/// ModifierKeys for the hotkey
		/// </summary>
		public ModifierKeys Modifiers {
			get;
			set;
		}

		void SettingsHotkeyTextBox_Loaded(object sender, RoutedEventArgs e) {
			_hotkeyValue = settingsPage.IniSections[ConfigPath][ConfigProperty];

			string shortcutConfig = _hotkeyValue.Value as string;
			Modifiers = HotkeyModifiersFromString(shortcutConfig);
			Hotkey = HotkeyFromString(shortcutConfig);
			Text = GetLocalizedHotkeyString(Modifiers, Hotkey);
		}

		private void ShortcutTextBox_PreviewKeyDown(object sender, KeyEventArgs e) {
			// The text box grabs all input.
			e.Handled = true;
			// Fetch the actual shortcut key.
			Key key = (e.Key == Key.System ? e.SystemKey : e.Key);

			// Ignore modifier keys.
			if (key == Key.LeftShift || key == Key.RightShift
				|| key == Key.LeftCtrl || key == Key.RightCtrl
				|| key == Key.LeftAlt || key == Key.RightAlt
				|| key == Key.LWin || key == Key.RWin) {
				return;
			}

			Hotkey = key;
			Modifiers = Keyboard.Modifiers;
			_hotkeyValue.Value = GetHotkeyString(Modifiers, Hotkey);
			// Update the text box.
			Text = GetLocalizedHotkeyString(Modifiers, Hotkey);
		}

		/// <summary>
		/// Helper method to convert the hotkey string directly to a localized string
		/// </summary>
		/// <param name="hotkeyString">config hotkey string</param>
		/// <returns>Localized hotkey string</returns>
		public static string GetLocalizedHotkeyStringFromString(string hotkeyString) {
			Key key = HotkeyFromString(hotkeyString);
			ModifierKeys modifiers = HotkeyModifiersFromString(hotkeyString);
			return GetLocalizedHotkeyString(modifiers, key);
		}

		/// <summary>
		/// Parse the string, and get the Key
		/// </summary>
		/// <param name="hotkeyString"></param>
		/// <returns>Key</returns>
		public static Key HotkeyFromString(string hotkeyString) {
			Key key = Key.None;
			try {
				if (!string.IsNullOrEmpty(hotkeyString)) {
					if (hotkeyString.LastIndexOf('+') > 0) {
						hotkeyString = hotkeyString.Remove(0, hotkeyString.LastIndexOf('+') + 1).Trim();
					}
					key = (Key)Enum.Parse(typeof(Key), hotkeyString);
				}
			} catch {
			}
			return key;
		}

		/// <summary>
		/// Parse the string, and get the ModifierKeys
		/// </summary>
		/// <param name="modifiersString"></param>
		/// <returns>ModifierKeys</returns>
		public static ModifierKeys HotkeyModifiersFromString(string modifiersString) {
			ModifierKeys modifiers = ModifierKeys.None;
			if (!string.IsNullOrEmpty(modifiersString)) {
				if (modifiersString.ToLower().Contains("alt")) {
					modifiers |= ModifierKeys.Alt;
				}
				if (modifiersString.ToLower().Contains("ctrl")) {
					modifiers |= ModifierKeys.Control;
				}
				if (modifiersString.ToLower().Contains("shift")) {
					modifiers |= ModifierKeys.Shift;
				}
				if (modifiersString.ToLower().Contains("win")) {
					modifiers |= ModifierKeys.Windows;
				}
			}
			return modifiers;
		}

		/// <summary>
		/// Get the Localized String for the supplied modifiers & hotkey
		/// </summary>
		/// <param name="modifiers"></param>
		/// <param name="hotkey"></param>
		/// <returns></returns>
		public static string GetLocalizedHotkeyString(ModifierKeys modifiers, Key hotkey) {
			// Build the shortcut key name.
			StringBuilder shortcutText = new StringBuilder();

			if ((modifiers & ModifierKeys.Control) != 0) {
				shortcutText.Append(GetKeyName(Key.LeftCtrl) + "+");
			}
			if ((modifiers & ModifierKeys.Shift) != 0) {
				shortcutText.Append(GetKeyName(Key.LeftShift) + "+");
			}
			if ((modifiers & ModifierKeys.Alt) != 0) {
				shortcutText.Append(GetKeyName(Key.LeftAlt) + "+");
			}
			// Fix snapshot
			if (hotkey == Key.Snapshot) {
				shortcutText.Append(GetKeyName(Key.PrintScreen));
			} else {
				shortcutText.Append(GetKeyName(hotkey));
			}
			return shortcutText.ToString();
		}

		/// <summary>
		/// Get the english (storage) String for the supplied modifiers & hotkey
		/// </summary>
		/// <param name="modifiers"></param>
		/// <param name="hotkey"></param>
		/// <returns></returns>
		public static string GetHotkeyString(ModifierKeys modifiers, Key hotkey) {
			StringBuilder shortcutConfig = new StringBuilder();
			// Build the shortcut key name.
			if ((modifiers & ModifierKeys.Control) != 0) {
				shortcutConfig.Append("Ctrl + ");
			}
			if ((modifiers & ModifierKeys.Shift) != 0) {
				shortcutConfig.Append("Shift + ");
			}
			if ((modifiers & ModifierKeys.Alt) != 0) {
				shortcutConfig.Append("Alt + ");
			}
			// Fix snapshot
			if (hotkey == Key.Snapshot) {
				shortcutConfig.Append("PrintScreen");
			} else {
				shortcutConfig.Append(hotkey.ToString());
			}
			return shortcutConfig.ToString();
		}

		/// <summary>
		/// Get the localized keyname
		/// </summary>
		/// <param name="givenKey"></param>
		/// <returns></returns>
		private static string GetKeyName(Key givenKey) {
			StringBuilder keyName = new StringBuilder();
			const uint NUMPAD = 55;

			Key virtualKey = givenKey;
			string keyString = "";
			// Make VC's to real keys
			switch (virtualKey) {
				case Key.Multiply:
					GetKeyNameText(NUMPAD << 16, keyName, 100);
					keyString = keyName.ToString().Replace("*", "").Trim().ToLower();
					if (keyString.IndexOf("(", System.StringComparison.Ordinal) >= 0) {
						return "* " + keyString;
					}
					keyString = keyString.Substring(0, 1).ToUpper() + keyString.Substring(1).ToLower();
					return keyString + " *";
				case Key.Divide:
					GetKeyNameText(NUMPAD << 16, keyName, 100);
					keyString = keyName.ToString().Replace("*", "").Trim().ToLower();
					if (keyString.IndexOf("(", System.StringComparison.Ordinal) >= 0) {
						return "/ " + keyString;
					}
					keyString = keyString.Substring(0, 1).ToUpper() + keyString.Substring(1).ToLower();
					return keyString + " /";
			}
			uint scanCode = MapVirtualKey((uint)KeyInterop.VirtualKeyFromKey(givenKey), (uint)MapType.MAPVK_VK_TO_VSC);

			// because MapVirtualKey strips the extended bit for some keys
			switch (virtualKey) {
				case Key.Left:
				case Key.Up:
				case Key.Right:
				case Key.Down: // arrow keys
				case Key.Prior:
				case Key.Next: // page up and page down
				case Key.End:
				case Key.Home:
				case Key.Insert:
				case Key.Delete:
				case Key.NumLock:
					LOG.Debug("Modifying Extended bit");
					scanCode |= 0x100; // set extended bit
					break;
				case Key.PrintScreen: // PrintScreen
					scanCode = 311;
					break;
				case Key.Pause: // PrintScreen
					scanCode = 69;
					break;
			}
			scanCode |= 0x200;
			if (GetKeyNameText(scanCode << 16, keyName, 100) != 0) {
				string visibleName = keyName.ToString();
				if (visibleName.Length > 1) {
					visibleName = visibleName.Substring(0, 1) + visibleName.Substring(1).ToLower();
				}
				return visibleName;
			}
			return givenKey.ToString();
		}
	}
}