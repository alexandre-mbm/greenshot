﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using GreenshotPlugin.Core;
using log4net;

namespace GreenshotPlugin.Controls {
	/// <summary>
	/// The OAuthLoginForm is used to allow the user to authorize Greenshot with an "Oauth" application
	/// </summary>
	public sealed partial class OAuthLoginForm : Form {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(OAuthLoginForm));
		private readonly string _callbackUrl;
		private IDictionary<string, string> _callbackParameters;
		
		public IDictionary<string, string> CallbackParameters {
			get { return _callbackParameters; }
		}
		
		public bool IsOk {
			get {
				return DialogResult == DialogResult.OK;
			}
		}
		
		public OAuthLoginForm(string browserTitle, Size size, string authorizationLink, string callbackUrl) {
			_callbackUrl = callbackUrl;
			InitializeComponent();
			ClientSize = size;
			Icon = GreenshotResources.GetGreenshotIcon();
			Text = browserTitle;
			addressTextBox.Text = authorizationLink;

			// The script errors are suppressed by using the ExtendedWebBrowser
			browser.ScriptErrorsSuppressed = false;
			browser.DocumentCompleted += browser_DocumentCompleted;
			browser.Navigate(new Uri(authorizationLink));

			WindowDetails.ToForeground(Handle);
		}

		private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {
			LOG.DebugFormat("document completed with url: {0}", browser.Url);
			CheckUrl();
		}

		private void CheckUrl() {
			if (browser.Url.ToString().StartsWith(_callbackUrl)) {
				string queryParams = browser.Url.Query;
				if (queryParams.Length > 0) {
					queryParams = NetworkHelper.UrlDecode(queryParams);
					//Store the Token and Token Secret
					_callbackParameters = NetworkHelper.ParseQueryString(queryParams);
				}
				DialogResult = DialogResult.OK;
			}
		}
	}

	/// <summary>
	/// As long as the OAuthLoginForm is still a form, we need a wrapper to prevent the usage of the system.windows.forms package 
	/// </summary>
	public static class OAuthLoginFormWrapper {
		public static IDictionary<string, string> ShowDialog(string browserTitle, Size size, string authorizationLink, string callbackUrl) {
			var loginForm = new OAuthLoginForm(browserTitle, size, authorizationLink, callbackUrl);
			loginForm.ShowDialog();
			if (loginForm.IsOk) {
				return loginForm.CallbackParameters;
			}
			return null;
		}
	}
}
