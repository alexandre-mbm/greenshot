﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Greenshot.TrayIcon;
using GreenshotPlugin.Modules;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace Greenshot.Model {
	/// <summary>
	/// This is the main ViewModel for Greenshot, it has everything that is needed to show the context menu etc
	/// Note: The WPF editor, which should come later, should have its own
	/// </summary>
	[Export]
	public class MainViewModel {
		private const int MAX_CONTEXT = 1;
		private MainViewModel() {
			ContextMenu = new ObservableCollection<ContextMenuItem>();
			Captures = new ObservableCollection<ExportLifetimeContext<CaptureContext>>();
			SvgImages = new ObservableDictionary<string, DrawingImage>();
		}

		[Export("ContextMenu")]
		public ObservableCollection<ContextMenuItem> ContextMenu {
			get;
			set;
		}

		[Export("Captures")]
		public ObservableCollection<ExportLifetimeContext<CaptureContext>> Captures {
			get;
			set;
		}

		/// <summary>
		/// Add a new CaptureContext (actuall the ExportLifetimeContext<CaptureContext>) to the collection.
		/// </summary>
		/// <param name="captureContextExport"></param>
		public void AddCaptureContext(ExportLifetimeContext<CaptureContext> captureContextExport) {
			while (Captures.Count >= MAX_CONTEXT) {
				var oldCaptureContextExport = Captures[0];
				Captures.RemoveAt(0);
				oldCaptureContextExport.Dispose();
			}
			Captures.Add(captureContextExport);
		}

		[Import]
		public Lazy<GreenshotTaskbarIcon> TaskbarIcon {
			get;
			set;
		}

		/// <summary>
		/// This is the list of all SvgImages, and is exported so everything can import this.
		/// Xaml binding can be made via e.g. Icon="{Binding SvgImages[greenshot]}" if this is imported.
		/// </summary>
		[Export("SvgImages")]
		public ObservableDictionary<string, DrawingImage> SvgImages {
			get;
			set;
		}
	}
}
