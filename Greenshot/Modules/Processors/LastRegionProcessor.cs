﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using GreenshotPlugin.UnmanagedHelpers;
using GreenshotPlugin.WPF;
using log4net;
using System.ComponentModel.Composition;
using System.Windows.Media;

namespace Greenshot.Modules {
	/// <summary>
	/// Use this processor to crop to the last used region, ofcouse this depends on the other processors to write the value!!
	/// </summary>
	[Processor(Designation = "LastRegionProcessor", LanguageKey = "")]
	public class LastRegionProcessor : IProcessor {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(LastRegionProcessor));

		[Import]
		private CoreConfiguration _conf = null;

		public void Process(CaptureContext captureContext) {
			if (!_conf.LastCapturedRegion.IsEmpty) {
				captureContext.CropRect = _conf.LastCapturedRegion;
				captureContext.ClipArea = new RectangleGeometry(_conf.LastCapturedRegion);
			}
		}
	}
}
