﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Windows;
using Greenshot.Windows;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using log4net;
using Microsoft.Win32;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using System.Collections.Generic;
using Greenshot.Model;

namespace Greenshot.Modules {
	/// <summary>
	/// This class implements the code for our hotkeys, the methods are exported and used for registration in the "HotkeyRegisterAction" 
	/// </summary>
	[Export]
	public class MainHotkeysContextmenu {
		private static readonly ILog _log = LogManager.GetLogger(typeof(MainHotkeysContextmenu));

		#region Imports
		[ImportMany(typeof(ISource), AllowRecomposition = true)]
		private IEnumerable<Lazy<ISource, ISourceMetadata>> _sources = null;

		[ImportMany(typeof(IProcessor), AllowRecomposition=true)]
		private IEnumerable<Lazy<IProcessor, IProcessorMetadata>> _processors = null;

		[ImportMany(typeof(IDestination), AllowRecomposition = true)]
		private IEnumerable<Lazy<IDestination, IDestinationMetadata>> _destinations = null;

		[Import]
		private CoreConfiguration _conf = null;

		[Import]
		private Lazy<SettingsWindow> _settingsWindow = null;

		[Import]
		private Lazy<AboutWindow> _aboutWindow = null;

		[Import]
		private ExportFactory<CaptureFlowAction> _captureFlowFactory = null;

		[Import]
		private ExportFactory<CaptureContext> _contextFactory = null;

		[Import]
		private Lazy<MainViewModel> _mainViewModel = null;

		#endregion

		/// <summary>
		/// Create capturecontext and add it to the main view model (can be used for a history)
		/// </summary>
		/// <returns>CaptureContext</returns>
		private CaptureContext CreateContext() {
			var captureContextExport = _contextFactory.CreateExport();
			_mainViewModel.Value.AddCaptureContext(captureContextExport);
			return captureContextExport.Value;
		}

		[Hotkey(ConfigurationKey = "RegionHotkey", MessageLanguageKey = "contextmenu_capturearea")]
		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_capturearea", IconFilename = "region", Location = 1)]
		public void CaptureRegion(object parameter) {
			// Create & execute the capture flow
			using (var captureFlowExport = _captureFlowFactory.CreateExport()) {
				var captureFlow = captureFlowExport.Value;

				// Set the context on the capture flow
				captureFlow.Context = CreateContext();

				// As we want multiple-sources (screen & mouse) use the stack source
				var stackSource = new StackSource();
				// Add the screen source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "ScreenSource" select source.Value).First());
				// Add the mouse source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "MouseSource" select source.Value).First());
				// set the stack source as source for the capture flow
				captureFlow.Source = stackSource;

				// set the ClipProcessor (is the one that shows the region selection):
				captureFlow.Processor = (from processor in _processors where processor.Metadata.Designation == "ClipProcessor" select processor.Value).First();

				// set the DestinationPicker as destination for the capture flow
				captureFlow.Destination = (from destination in _destinations where destination.Metadata.Designation == "DestinationPicker" select destination.Value).First();

				// and GO!!!
				captureFlow.Execute();
			}
		}

		[Hotkey(ConfigurationKey = "FullscreenHotkey", MessageLanguageKey = "contextmenu_capturefullscreen")]
		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_capturefullscreen", IconFilename = "screen", Location = 1)]
		public void CaptureFullScreen(object parameter) {
			// Create & execute the capture flow
			using (var captureFlowExport = _captureFlowFactory.CreateExport()) {
				var captureFlow = captureFlowExport.Value;

				// Set the context on the capture flow
				captureFlow.Context = CreateContext();

				// As we want multiple-sources (screen & mouse) use the stack source
				var stackSource = new StackSource();
				// Add the screen source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "ScreenSource" select source.Value).First());
				// Add the mouse source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "MouseSource" select source.Value).First());
				// set the stack source as source for the capture flow
				captureFlow.Source = stackSource;

				// set the ScreenCaptureProcessor
				captureFlow.Processor = (from processor in _processors where processor.Metadata.Designation == "ScreenCaptureProcessor" select processor.Value).First();

				// set the DestinationPicker as destination for the capture flow
				captureFlow.Destination = (from destination in _destinations where destination.Metadata.Designation == "DestinationPicker" select destination.Value).First();

				// and GO!!!
				captureFlow.Execute();
			}
		}

		/// <summary>
		/// This entry can enable / disable the context menu entry for the clipboard.
		/// </summary>
		/// <param name="parameter">command parameter</param>
		/// <returns>false if the entry needs to be disabled</returns>
		[ContextMenuCanExecute(HeaderLanguageKey = "contextmenu_capturelastregion")]
		public bool CaptureLastRegionCanExecute(object parameter) {
			return !_conf.LastCapturedRegion.IsEmpty;
		}

		[Hotkey(ConfigurationKey = "LastregionHotkey", MessageLanguageKey = "contextmenu_capturelastregion")]
		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_capturelastregion", IconFilename = "last_region", Location = 1)]
		public void CaptureLastRegion(object parameter) {
			// Create & execute the capture flow
			using (var captureFlowExport = _captureFlowFactory.CreateExport()) {
				var captureFlow = captureFlowExport.Value;

				// Set the context on the capture flow
				captureFlow.Context = CreateContext();

				// As we want multiple-sources (screen & mouse) use the stack source
				var stackSource = new StackSource();
				// Add the screen source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "ScreenSource" select source.Value).First());
				// Add the mouse source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "MouseSource" select source.Value).First());
				// set the stack source as source for the capture flow
				captureFlow.Source = stackSource;

				// set the LastRegionProcessor
				captureFlow.Processor = (from processor in _processors where processor.Metadata.Designation == "LastRegionProcessor" select processor.Value).First();

				// set the DestinationPicker as destination for the capture flow
				captureFlow.Destination = (from destination in _destinations where destination.Metadata.Designation == "DestinationPicker" select destination.Value).First();

				// and GO!!!
				captureFlow.Execute();
			}
		}

		[Hotkey(ConfigurationKey = "IEHotkey", MessageLanguageKey = "contextmenu_captureie")]
		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_captureie", IconFilename = "ie", Location = 1)]
		public void CaptureIE(object parameter) {
			if (_conf.IECapture) {
				//CaptureHelper.CaptureIE(true, null);
			}
		}

		[Hotkey(ConfigurationKey = "WindowHotkey", MessageLanguageKey = "contextmenu_capturewindow")]
		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_capturewindow", IconFilename = "windows", Location = 1)]
		public void CaptureWindow(object parameter) {
			//if (_conf.CaptureWindowsInteractive) {
			//	CaptureHelper.CaptureWindowInteractive(true);
			//} else {

			// Create & execute the capture flow
			using (var captureFlowExport = _captureFlowFactory.CreateExport()) {
				var captureFlow = captureFlowExport.Value;

				// Set the context on the capture flow
				captureFlow.Context = CreateContext();

				// As we want multiple-sources (screen & mouse) use the stack source
				var stackSource = new StackSource();
				// Add the screen source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "ScreenSource" select source.Value).First());
				// Add the mouse source
				stackSource.Sources.Add((from source in _sources where source.Metadata.Designation == "MouseSource" select source.Value).First());
				// set the stack source as source for the capture flow
				captureFlow.Source = stackSource;

				// set the LastRegionProcessor
				captureFlow.Processor = (from processor in _processors where processor.Metadata.Designation == "ActiveWindowProcessor" select processor.Value).First();

				// set the DestinationPicker as destination for the capture flow
				captureFlow.Destination = (from destination in _destinations where destination.Metadata.Designation == "DestinationPicker" select destination.Value).First();

				// and GO!!!
				captureFlow.Execute();
			}
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_captureclipboard", IconFilename = "clipboard", Location = 3)]
		public void Clipboard(object parameter) {
			//CaptureHelper.CaptureClipboard();
		}

		/// <summary>
		/// This entry can enable / disable the context menu entry for the clipboard.
		/// </summary>
		/// <param name="parameter">command parameter</param>
		/// <returns>false if the entry needs to be disabled</returns>
		[ContextMenuCanExecute(HeaderLanguageKey = "contextmenu_captureclipboard")]
		public bool ClipboardCanExecute(object parameter) {
			return ClipboardHelper.ContainsImage();
		}


		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_openfile", IconFilename = "file", Location = 3)]
		public void OpenFile(object parameter) {
			var openFileDialog = new OpenFileDialog {
				Filter = @"Image files (*.greenshot, *.png, *.jpg, *.gif, *.bmp, *.ico, *.tiff, *.wmf)|*.greenshot; *.png; *.jpg; *.jpeg; *.gif; *.bmp; *.ico; *.tiff; *.tif; *.wmf",
				DefaultExt = ".png"
			};
			if ((bool)openFileDialog.ShowDialog()) {
				if (File.Exists(openFileDialog.FileName)) {
					//CaptureHelper.CaptureFile(openFileDialog.FileName);
				}
			}
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_openrecentcapture", Location = 4)]
		public void OpenRecent(object parameter) {
			string path = FilenameHelper.FillVariables(_conf.OutputFilePath, false);
			// Fix for #1470, problems with a drive which is no longer available
			try {
				string lastFilePath = Path.GetDirectoryName(_conf.OutputFileAsFullpath);
				if (lastFilePath != null && Directory.Exists(lastFilePath)) {
					path = lastFilePath;
				} else if (!Directory.Exists(path)) {
					// What do I open when nothing can be found? Right, nothing...
					return;
				}
			} catch (Exception ex) {
				_log.Warn("Couldn't open the path to the last exported file, taking default.", ex);
			}
			_log.Debug("DoubleClick was called! Starting: " + path);
			try {
				Process.Start(path);
			} catch (Exception ex) {
				// Make sure we show what we tried to open in the exception
				ex.Data.Add("path", path);
				_log.Warn("Couldn't open the path to the last exported file", ex);
				// No reason to create a bug-form, we just display the error.
				MessageBox.Show(ex.Message, @"Opening " + path, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_settings", IconFilename = "settings", Location = 6)]
		public void ShowSettings(object parameter) {
			if (!_settingsWindow.Value.IsVisible) {
				_settingsWindow.Value.Show();
			} else {
				_settingsWindow.Value.Activate();
			}
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_help", Location = 7)]
		public void ShowHelp(object parameter) {
			HelpFileLoader.LoadHelp();
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_donate", IconFilename = "supportus", Location = 7)]
		public void SupportUs(object parameter) {
			Process.Start("http://getgreenshot.org/support/");
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_about", IconFilename = "about", Location = 7)]
		public void ShowAbout(object parameter) {
			if (!_aboutWindow.Value.IsVisible) {
				_aboutWindow.Value.Show();
			} else {
				_aboutWindow.Value.Activate();
			}
		}

		[ContextMenuExecute(HeaderLanguageKey = "contextmenu_exit", IconFilename = "exit", Location = 8)]
		public void Exit(object parameter) {
			Application.Current.Shutdown();
		}

		// TODO: Hotkeys in the context menu
		//	contextmenu_capturearea.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.RegionHotkey);
		//	contextmenu_capturelastregion.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.LastregionHotkey);
		//	contextmenu_capturewindow.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.WindowHotkey);
		//	contextmenu_capturefullscreen.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.FullscreenHotkey);
		//	contextmenu_captureie.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.IEHotkey);
		// TODO: Enable / disable depending on state:
			//contextmenu_captureclipboard.Enabled = ClipboardHelper.ContainsImage();
			//contextmenu_capturelastregion.Enabled = _conf.LastCapturedRegion != Rectangle.Empty;

			//// IE context menu code
			//try {
			//	if (_conf.IECapture && IECaptureHelper.IsIERunning()) {
			//		contextmenu_captureie.Enabled = true;
			//		contextmenu_captureiefromlist.Enabled = true;
			//	} else {
			//		contextmenu_captureie.Enabled = false;
			//		contextmenu_captureiefromlist.Enabled = false;
			//	}
			//} catch (Exception ex) {
			//	_log.WarnFormat("Problem accessing IE information: {0}", ex.Message);
			//}

		// TODO: Complete context menu, following translations were used in the old
		//<resource name="contextmenu_about">About Greenshot</resource>
		//<resource name="contextmenu_capturearea">Capture region</resource>
		//<resource name="contextmenu_captureclipboard">Open image from clipboard</resource>
		//<resource name="contextmenu_capturefullscreen">Capture full screen</resource>
		//<resource name="contextmenu_capturefullscreen_all">all</resource>
		//<resource name="contextmenu_capturefullscreen_bottom">bottom</resource>
		//<resource name="contextmenu_capturefullscreen_left">left</resource>
		//<resource name="contextmenu_capturefullscreen_right">right</resource>
		//<resource name="contextmenu_capturefullscreen_top">top</resource>
		//<resource name="contextmenu_captureie">Capture Internet Explorer</resource>
		//<resource name="contextmenu_captureiefromlist">Capture Internet Explorer from list</resource>
		//<resource name="contextmenu_capturelastregion">Capture last region</resource>
		//<resource name="contextmenu_capturewindow">Capture window</resource>
		//<resource name="contextmenu_capturewindowfromlist">Capture window from list</resource>
		//<resource name="contextmenu_donate">Support Greenshot</resource>
		//<resource name="contextmenu_exit">Exit</resource>
		//<resource name="contextmenu_help">Help</resource>
		//<resource name="contextmenu_openfile">Open image from file</resource>
		//<resource name="contextmenu_openrecentcapture">Open last capture location</resource>
		//<resource name="contextmenu_quicksettings">Quick preferences</resource>
		//<resource name="contextmenu_settings">Preferences...</resource>
	}
}
