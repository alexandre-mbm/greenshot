﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Greenshot.Model;
using GreenshotPlugin.Modules;
using log4net;
using Svg2Xaml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;

namespace Greenshot.Modules {
	/// <summary>
	/// This startup action does the initial context-menu setup
	/// </summary>
	[StartupAction(StartupOrder = 5)]
	public class ContextMenuSetupAction : IStartupAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ContextMenuSetupAction));

		[ImportMany(typeof(IContextMenuExecuteItem))]
		private IEnumerable<Lazy<Action<object>, IContextMenuItemExecuteMetadata>> _contextMenuExecuteItems = null;
		[ImportMany(typeof(IContextMenuCanExecuteItem))]
		private IEnumerable<Lazy<Func<object, bool>, IContextMenuItemCanExecuteMetadata>> _contextMenuCanExecuteItems = null;

		[Import("ContextMenu")]
		private ObservableCollection<ContextMenuItem> _contextMenu = null;
		[Import("SvgImages")]
		private ObservableDictionary<string, DrawingImage> _svgImages = null;

		public void Start() {
			var orderedItems = from contextMenuExecuteItem in _contextMenuExecuteItems orderby contextMenuExecuteItem.Metadata.Location select contextMenuExecuteItem;
			int previousLocation = orderedItems.First().Metadata.Location;
			foreach (var contextMenuExecuteItem in orderedItems) {
				// Separator between every location change
				if (previousLocation != contextMenuExecuteItem.Metadata.Location) {
					previousLocation = contextMenuExecuteItem.Metadata.Location;
					_contextMenu.Add(null);
				}
				Image iconImage = null;
				var iconFilename = contextMenuExecuteItem.Metadata.IconFilename;
				if (!string.IsNullOrWhiteSpace(iconFilename)) {
					try {
						// Cache the loading
						if (!_svgImages.ContainsKey(iconFilename)) {
							_svgImages.Add(iconFilename, SvgReader.Load(this.GetType(), iconFilename));
						}
						iconImage = new Image();
						iconImage.BeginInit();
						iconImage.Source = _svgImages[iconFilename];
						iconImage.Width = 16;
						iconImage.Height = 16;
						iconImage.EndInit();
					} catch {
						LOG.WarnFormat("Couldn't find icon file {0}", iconFilename);
					}
				}
				var matchingCanExecuteItem = (from contextMenuCanExecuteItem in _contextMenuCanExecuteItems where contextMenuCanExecuteItem.Metadata.HeaderLanguageKey == contextMenuExecuteItem.Metadata.HeaderLanguageKey select contextMenuCanExecuteItem).FirstOrDefault();

				ActionCommand itemCommand;
				if (matchingCanExecuteItem != null) {
					itemCommand = new ActionCommand(contextMenuExecuteItem.Value, matchingCanExecuteItem.Value);
				} else {
					itemCommand = new ActionCommand(contextMenuExecuteItem.Value);
				}
				_contextMenu.Add(new ContextMenuItem {
					Key = contextMenuExecuteItem.Metadata.HeaderLanguageKey,
					Icon = iconImage,
					ItemCommand = itemCommand
				});
			}

			//// Add separator
			//mainViewModel.ContextMenu.Add(null);

			//var subItems = new ObservableCollection<ContextMenuItem>();
			//subItems.Add(new ContextMenuItem {
			//	Header = "Level 2 (does exit)",
			//	ItemCommand = new Command(() => {
			//		Application.Current.Shutdown();
			//		System.Windows.Forms.Application.Exit();
			//	}),
			//	Icon = icon
			//});
			//mainViewModel.ContextMenu.Add(new ContextMenuItem {
			//	Header = "Level 1",
			//	SubItems = subItems
			//});
			//// Add separator
			//mainViewModel.ContextMenu.Add(null);
			//mainViewModel.ContextMenu.Add(new ContextMenuItem {
			//	Key = "contextmenu_exit",
			//	ItemCommand = () => {
			//		Application.Current.Shutdown();
			//		System.Windows.Forms.Application.Exit();
			//	}
			//});

		}



	}
}
