﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Modules;
using log4net;
using Svg2Xaml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.ComponentModel.Composition;
using System.IO;
using System.Windows.Media;

namespace Greenshot.Modules {
	/// <summary>
	/// Load all the icons into the MainViewModel
	/// </summary>
	[StartupAction(StartupOrder = -10)]
	public class IconLoaderAction : IStartupAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(IconLoaderAction));

		[Import("SvgImages")]
		private ObservableDictionary<string, DrawingImage> _svgImages = null;

		[Import("PluginPaths", typeof(ISet<string>))]
		private ISet<string> _pluginPaths = null;

		public void Start() {
			var assemblyLocation = Path.GetDirectoryName(GetType().Assembly.Location);
			// Load the greenshot SVG Logo, which is used throughout the application
			if (assemblyLocation != null) {
				var svgDirectory = Path.Combine(assemblyLocation, "Icons");

				LoadIcons(GetType(), svgDirectory);
			}
			foreach (var pluginDirectory in _pluginPaths) {
				var iconDirectory = Path.Combine(pluginDirectory, "Icons");
				if (Directory.Exists(iconDirectory)) {
					LoadIcons(null, iconDirectory);
				}
			}
		}

		/// <summary>
		/// Load all the svg* files from the supplied directory 
		/// </summary>
		/// <param name="baseType"></param>
		/// <param name="svgDirectory"></param>
		private void LoadIcons(Type baseType, string svgDirectory) {
			foreach (var filepath in Directory.EnumerateFiles(svgDirectory, "*.svg*")) {
				var filename = Path.GetFileNameWithoutExtension(filepath);
				if (!_svgImages.ContainsKey(filename)) {
					try {
						if (baseType != null) {
							_svgImages[filename] = SvgReader.Load(GetType(), filename);
						} else {
							_svgImages[filename] = SvgReader.Load(filepath);
						}
						LOG.DebugFormat("Added file {0} to Svg-cache", filename);
					} catch {
						LOG.WarnFormat("Couldn't read file {0}", filename);
					}
				} else {
					LOG.WarnFormat("found double for {0} at {1}", filename, filepath);
				}
			}
		}
	}
}
