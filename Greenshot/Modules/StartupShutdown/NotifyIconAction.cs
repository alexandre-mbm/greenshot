﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using Greenshot.TrayIcon;
using System.Windows.Controls.Primitives;
using Greenshot.Model;
using System;
using GreenshotPlugin.Core;

namespace Greenshot.Modules {
	/// <summary>
	/// Create and show the Taskbar Icon
	/// </summary>
	[StartupAction(StartupOrder = -9)]
	public class NotifyIconAction : IStartupAction {

		[Import]
		private CoreConfiguration _config = null;

		[Import]
		private Lazy<MainViewModel> _mainViewModel = null;

		[Import]
		private Lazy<FirstStartBalloon> _firstStartBalloon = null;
		
		public void Start() {
			if (!_config.IsFirstLaunch) {
				_config.IsFirstLaunch = false;
				//IniConfig.Save();

				_mainViewModel.Value.TaskbarIcon.Value.ShowCustomBalloon(_firstStartBalloon.Value, PopupAnimation.Slide, 10000);
			}
		}
	}
}
