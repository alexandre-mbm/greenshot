﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using GreenshotPlugin.Core;
using Greenshot.Windows;
using System;

namespace Greenshot.Modules {
	/// <summary>
	/// If no language is set, we will need to ask the language from the user
	/// </summary>
	[StartupAction(StartupOrder = 0)]
	public class LanguageCheckAction : IStartupAction {

		[Import]
		CoreConfiguration _conf = null;

		[Import]
		private Lazy<LanguageSelectionWindow> _languageSelectionWindow = null;

		public void Start() {
			// if language is not set, show language dialog
			if (string.IsNullOrEmpty(_conf.Language)) {
				// Don't show when there is only one language available, than we just take that
				if (Language.SupportedLanguages.Count == 1) {
					_conf.Language = Language.SupportedLanguages[0].Ietf;
				} else {
					_languageSelectionWindow.Value.ShowDialog();
					_conf.Language = _languageSelectionWindow.Value.SelectedLanguage.Ietf;
				}
				// TODO: Save?
				// IniConfig.Save();
			}
		}
	}
}
