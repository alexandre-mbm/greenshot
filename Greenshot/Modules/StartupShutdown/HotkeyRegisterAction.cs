﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Greenshot.Configuration;
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Greenshot.Modules {
	/// <summary>
	/// This class will be automatically loaded and executed at startup
	/// </summary>
	[StartupAction(StartupOrder = 2)]
	[ShutdownAction(ShutdownOrder = -1)]
	public class HotkeyRegisterAction : IStartupAction, IShutdownAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(HotkeyRegisterAction));

		#region Native Code
		[DllImport("user32.dll", EntryPoint = "GetKeyNameTextW", SetLastError = true, CharSet = CharSet.Unicode)]
		private static extern int GetKeyNameText(uint lParam, [Out] StringBuilder lpString, int nSize);
		[DllImport("user32.dll", EntryPoint = "MapVirtualKeyW", SetLastError = true, CharSet = CharSet.Unicode)]
		private static extern uint MapVirtualKey(uint uCode, uint uMapType);
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint virtualKeyCode);
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
		#endregion

		private readonly Dictionary<int, Action<object>> _keyHandlers = new Dictionary<int, Action<object>>();
		private int _hotKeyCounter = 1;

		[ImportMany]
		private IEnumerable<Lazy<IniSection>> _iniSections = null;

		[Import]
		private NativeGreenshotWindow _nativeWindow = null;

		/// <summary>
		/// All the methods that have [Export("HotkeyFunction")] and are of type "bool Methodname()"
		/// </summary>
		[ImportMany(typeof(IHotkey))]
		private IEnumerable<Lazy<Action<object>, IHotkeyMetadata>> _hotkeyExports {
			get;
			set;
		}

		/// <summary>
		/// Called when the application does its startup
		/// </summary>
		public void Start() {
			_nativeWindow.HotkeyEvent += OnHotkey;
			RegisterHotkeys(false);
		}

		/// <summary>
		/// Called when the application shuts down
		/// </summary>
		public void Shutdown() {
			UnregisterHotkeys();
		}

		/// <summary>
		/// Here the hotkey is called, this is an event handler
		/// </summary>
		/// <param name="hotkeyRegistrationNr"></param>
		private void OnHotkey(int hotkeyRegistrationNr) {
			if (_keyHandlers.ContainsKey(hotkeyRegistrationNr)) {
				// what the parameter is for, is currently is not decided. Type is "object", due to a ICommand interface Action<object> restriction
				_keyHandlers[hotkeyRegistrationNr](null);
			}
		}
		/// <summary>
		/// Displays a dialog for the user to choose how to handle hotkey registration failures: 
		/// retry (allowing to shut down the conflicting application before),
		/// ignore (not registering the conflicting hotkey and resetting the respective config to "None", i.e. not trying to register it again on next startup)
		/// abort (do nothing about it)
		/// </summary>
		/// <param name="failedKeys">comma separated list of the hotkeys that could not be registered, for display in dialog text</param>
		/// <returns></returns>
		private bool HandleFailedHotkeyRegistration(string failedKeys) {
			bool success = false;

			string text = Language.GetFormattedString(LangKey.warning_hotkeys, failedKeys);
			string caption = Language.GetString(LangKey.warning);
			var result = MessageBox.Show(text, caption, MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
			if (result == MessageBoxResult.OK) {
				LOG.DebugFormat("Re-trying to register hotkeys");
				UnregisterHotkeys();
				success = RegisterHotkeys(false);
			} else if (result == MessageBoxResult.Cancel) {
				LOG.DebugFormat("Ignoring failed hotkey registration");
				UnregisterHotkeys();
				success = RegisterHotkeys(true);
			}
			return success;
		}

		/// <summary>
		/// Registers all hotkeys as configured, displaying a dialog in case of hotkey conflicts with other tools.
		/// </summary>
		/// <param name="ignoreFailedRegistration">if true, a failed hotkey registration will not be reported to the user - the hotkey will simply not be registered</param>
		/// <returns>Whether the hotkeys could be registered to the users content. This also applies if conflicts arise and the user decides to ignore these (i.e. not to register the conflicting hotkey).</returns>
		private bool RegisterHotkeys(bool ignoreFailedRegistration) {
			bool success = true;
			StringBuilder failedKeys = new StringBuilder();

			foreach (var hotkeyExport in _hotkeyExports) {

				if (!RegisterWrapper(failedKeys, hotkeyExport, ignoreFailedRegistration)) {
					success = false;
				}
			}

			if (!success) {
				if (!ignoreFailedRegistration) {
					success = HandleFailedHotkeyRegistration(failedKeys.ToString());
				} else {
					// if failures have been ignored, the config has probably been updated
					// TODO: Save??
					//IniConfig.Save();
				}
			}
			return success || ignoreFailedRegistration;
		}

		/// <summary>
		/// Register
		/// </summary>
		/// <param name="failedKeys"></param>
		/// <param name="hotkeyExport"></param>
		/// <param name="ignoreFailedRegistration"></param>
		/// <returns></returns>
		private bool RegisterWrapper(StringBuilder failedKeys, Lazy<Action<object>, IHotkeyMetadata> hotkeyExport, bool ignoreFailedRegistration) {
			IniSection section = null;
			foreach (var possibleSection in _iniSections) {
				if (hotkeyExport.Metadata.ConfigurationSection.Equals(possibleSection.Value.Name)) {
					section = possibleSection.Value;
					break;
				}
			}
			if (section == null) {
				throw new InvalidOperationException(string.Format("Can't find section {0}", hotkeyExport.Metadata.ConfigurationSection));
			}
			IniValue hotkeyValue = section.Values[hotkeyExport.Metadata.ConfigurationKey];
			try {
				bool success = RegisterHotKey(hotkeyValue.Value.ToString(), hotkeyExport.Value);

				if (!success && ignoreFailedRegistration) {
					LOG.DebugFormat("Ignoring failed hotkey registration, resetting {0}.{1} to 'None'.", hotkeyExport.Metadata.ConfigurationSection, hotkeyExport.Metadata.ConfigurationKey);
					hotkeyValue.Value = System.Windows.Forms.Keys.None.ToString();
					hotkeyValue.ContainingIniSection.IsDirty = true;
				} else if (!success) {
					LOG.DebugFormat("Failed to register {0} to hotkey: {1}", Language.GetString(hotkeyExport.Metadata.MessageLanguageKey), hotkeyValue.Value);
					if (failedKeys.Length > 0) {
						failedKeys.Append(", ");
					}
					failedKeys.Append(Language.GetString(hotkeyExport.Metadata.MessageLanguageKey));
				} else {
					LOG.DebugFormat("Registered {0} to hotkey: {1}", Language.GetString(hotkeyExport.Metadata.MessageLanguageKey), hotkeyValue.Value);
				}
				return success;
			} catch (Exception ex) {
				LOG.Warn(ex);
				LOG.WarnFormat("Restoring default hotkey for {0}, stored under {1}.{2} from '{3}' to '{4}'", Language.GetString(hotkeyExport.Metadata.MessageLanguageKey), hotkeyExport.Metadata.ConfigurationSection, hotkeyExport.Metadata.ConfigurationKey, hotkeyValue.Value, hotkeyValue.Attributes.DefaultValue);
				// when getting an exception the key wasn't found: reset the hotkey value
				hotkeyValue.UseValueOrDefault(null);
				hotkeyValue.ContainingIniSection.IsDirty = true;
				return RegisterHotKey(hotkeyValue.Value.ToString(), hotkeyExport.Value);
			}
		}

		/// <summary>
		/// Register a hotkey
		/// </summary>
		/// <param name="hWnd">The window which will get the event</param>
		/// <param name="modifierKeyCode">The modifier, e.g.: Modifiers.CTRL, Modifiers.NONE or Modifiers.ALT</param>
		/// <param name="virtualKeyCode">The virtual key code</param>
		/// <param name="handler">A HotKeyHandler, this will be called to handle the hotkey press</param>
		/// <returns>the hotkey number, -1 if failed</returns>
		private bool RegisterHotKey(string hotkeyString, Action<object> handler) {
			ModifierKeys modifiers = HotkeyModifiersFromString(hotkeyString);
			Key key = HotkeyFromString(hotkeyString);
			if (key == Key.None) {
				LOG.Warn("Trying to register a Keys.none hotkey, ignoring");
				return true;
			}

			if (RegisterHotKey(_nativeWindow.Handle, _hotKeyCounter, (uint)modifiers, (uint)KeyInterop.VirtualKeyFromKey(key))) {
				_keyHandlers.Add(_hotKeyCounter++, handler);
				return true;
			}
			LOG.Warn(String.Format("Couldn't register hotkey modifier {0} key {1}", modifiers, key));
			return false;
		}

		/// <summary>
		/// Unregister all the hotkeys
		/// </summary>
		private void UnregisterHotkeys() {
			foreach (int hotkey in _keyHandlers.Keys) {
				UnregisterHotKey(_nativeWindow.Handle, hotkey);
			}
			// Remove all key handlers
			_keyHandlers.Clear();
		}


		/// <summary>
		/// Parse the string, and get the ModifierKeys
		/// </summary>
		/// <param name="modifiersString"></param>
		/// <returns>ModifierKeys</returns>
		public static ModifierKeys HotkeyModifiersFromString(string modifiersString) {
			ModifierKeys modifiers = ModifierKeys.None;
			if (!string.IsNullOrEmpty(modifiersString)) {
				if (modifiersString.ToLower().Contains("alt")) {
					modifiers |= ModifierKeys.Alt;
				}
				if (modifiersString.ToLower().Contains("ctrl")) {
					modifiers |= ModifierKeys.Control;
				}
				if (modifiersString.ToLower().Contains("shift")) {
					modifiers |= ModifierKeys.Shift;
				}
				if (modifiersString.ToLower().Contains("win")) {
					modifiers |= ModifierKeys.Windows;
				}
			}
			return modifiers;
		}

		/// <summary>
		/// Parse the string, and get the Key
		/// </summary>
		/// <param name="hotkeyString"></param>
		/// <returns>Key</returns>
		public static Key HotkeyFromString(string hotkeyString) {
			Key key = Key.None;
			try {
				if (!string.IsNullOrEmpty(hotkeyString)) {
					if (hotkeyString.LastIndexOf('+') > 0) {
						hotkeyString = hotkeyString.Remove(0, hotkeyString.LastIndexOf('+') + 1).Trim();
					}
					key = (Key)Key.Parse(typeof(Key), hotkeyString);
				}
			} catch {
			}
			return key;
		}
	}
}
