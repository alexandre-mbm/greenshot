﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Modules;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace Greenshot.Modules {
	/// <summary>
	/// The StartupShutdownActionTrigger calls all the IStartupAction & IShutdownAction actions
	/// </summary>
	[Export(typeof(IStartupShutdownTrigger))]
	public class StartupShutdownActionTrigger : IStartupShutdownTrigger {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(StartupShutdownActionTrigger));

		[ImportMany]
		private IEnumerable<Lazy<IStartupAction, IStartupActionMetadata>> _startupActions = null;

		[ImportMany]
		private IEnumerable<Lazy<IShutdownAction, IShutdownActionMetadata>> _shutdownActions = null;

		public void Startup() {
			LOG.Debug("Starting IStartupActions");
			// Startup all "Startup actions"
			var orderedActions = from export in _startupActions orderby export.Metadata.StartupOrder ascending select export;

			foreach (var startupAction in orderedActions) {
				try {
					LOG.InfoFormat("Starting: {0}", startupAction.Value.GetType());
					startupAction.Value.Start();
				} catch (Exception ex) {
					if (startupAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing startupAction {0}: ", startupAction.Value.GetType()), ex);
					} else {
						LOG.Error("Exception instantiating startupAction: ", ex);
					}
				}
			}
		}

		public void Shutdown() {
			LOG.Debug("Shutting down IShutdownActions");
			// Initiate Shutdown on all "Shutdown actions"
			var orderedActions = from export in _shutdownActions orderby export.Metadata.ShutdownOrder ascending select export;
			foreach (var shutdownAction in orderedActions) {
				try {
					LOG.InfoFormat("Shutting down: {0}", shutdownAction.Value.GetType());
					shutdownAction.Value.Shutdown();
				} catch (Exception ex) {
					if (shutdownAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing shutdownAction {0}: ", shutdownAction.Value.GetType()), ex);
					} else {
						LOG.Error("Exception instantiating shutdownAction: ", ex);
					}
				}
			}
		}
	}
}
