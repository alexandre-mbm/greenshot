﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Greenshot.IniFile;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Greenshot.Modules {
	/// <summary>
	/// Load an process the ini file
	/// </summary>
	[StartupAction(StartupOrder = -10)]
	public class IniConfigAction : IStartupAction, IShutdownAction, IPartImportsSatisfiedNotification {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(IniConfigAction));
		private const string IniExtension = ".ini";
		private const string DefaultsPostfix = "-defaults";
		private const string FixedPostfix = "-fixed";
		private const string ConstantsPostfix = "-constants";

		[ImportMany]
		private IEnumerable<Lazy<IniSection>> _iniSections = null;

		[Import]
		private CompositionContainer _container = null;

		/// <summary>
		/// Make the ini proxies as soon as possible available for importing
		/// </summary>
		public void OnImportsSatisfied() {
			// Make the ini proxies available for importing
			IDictionary<string, IniProxy> iniProxies = new Dictionary<string, IniProxy>();
			foreach (var iniSection in _iniSections) {
				iniProxies.Add(iniSection.Value.Name, new IniProxy(iniSection.Value));
			}
			_container.ComposeExportedValue<IDictionary<string, IniProxy>>(iniProxies);
		}

		public void Start() {
			Reload();
		}

		public void Shutdown() {
			Save();
		}


		/// <summary>
		/// A lock object for the ini file saving
		/// </summary>
		private readonly object IniLock = new object();

		/// <summary>
		/// A Dictionary with the properties for a section stored by section name
		/// </summary>
		private readonly IDictionary<string, Dictionary<string, string>> _sections = new Dictionary<string, Dictionary<string, string>>();

		/// <summary>
		/// A Dictionary with the fixed-properties for a section stored by section name
		/// </summary>
		private IDictionary<string, Dictionary<string, string>> _fixedProperties;

		/// <summary>
		/// Config directory when set from external
		/// </summary>
		public string IniDirectory {
			get;
			set;
		}

		/// <summary>
		/// Get the location of the configuration
		/// </summary>
		public string ConfigLocation {
			get {
				return CreateIniLocation(CoreConfiguration.ApplicationName + IniExtension, false);
			}
		}

		/// <summary>
		/// Create the location of the configuration file
		/// </summary>
		private string CreateIniLocation(string configFilename, bool isReadOnly) {
			string iniFilePath = null;

			// Check if a Ini-Directory was supplied, and it's valid, use this before any others.
			try {
				if (IniDirectory != null && Directory.Exists(IniDirectory)) {
					// If the greenshot.ini is requested, use the supplied directory even if empty
					if (!isReadOnly) {
						return Path.Combine(IniDirectory, configFilename);
					}
					iniFilePath = Path.Combine(IniDirectory, configFilename);
					if (File.Exists(iniFilePath)) {
						return iniFilePath;
					}
					iniFilePath = null;
				}
			} catch (Exception exception) {
				LOG.WarnFormat("The ini-directory {0} can't be used due to: {1}", IniDirectory, exception.Message);
			}

			string applicationStartupPath;
			try {
				applicationStartupPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			} catch (Exception exception) {
				LOG.WarnFormat("Problem retrieving the AssemblyLocation: {0} (Designer mode?)", exception.Message);
				applicationStartupPath = @".";
			}
			if (applicationStartupPath != null) {
				string pafPath = Path.Combine(applicationStartupPath, @"App\" + CoreConfiguration.ApplicationName);

				if (CoreConfiguration.IsPortable) {
					string pafConfigPath = Path.Combine(applicationStartupPath, @"Data\Settings");
					try {
						if (!Directory.Exists(pafConfigPath)) {
							Directory.CreateDirectory(pafConfigPath);
						}
						iniFilePath = Path.Combine(pafConfigPath, configFilename);
					} catch (Exception e) {
						LOG.InfoFormat("Portable mode NOT possible, couldn't create directory '{0}'! Reason: {1}", pafConfigPath, e.Message);
					}
				}
			}
			if (iniFilePath == null) {
				// check if file is in the same location as started from, if this is the case
				// we will use this file instead of the Applicationdate folder
				// Done for Feature Request #2741508
				if (applicationStartupPath != null) {
					iniFilePath = Path.Combine(applicationStartupPath, configFilename);
				}
				if (!File.Exists(iniFilePath)) {
					string iniDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), CoreConfiguration.ApplicationName);
					if (!Directory.Exists(iniDirectory)) {
						Directory.CreateDirectory(iniDirectory);
					}
					iniFilePath = Path.Combine(iniDirectory, configFilename);
				}
			}
			LOG.InfoFormat("Using ini file {0}", iniFilePath);
			return iniFilePath;
		}

		/// <summary>
		/// Reload the Ini file
		/// </summary>
		public void Reload() {
			// Clear the current properties
			_sections.Clear();
			// Load the defaults
			Read(CreateIniLocation(CoreConfiguration.ApplicationName + DefaultsPostfix + IniExtension, true));
			// Load the normal
			Read(CreateIniLocation(CoreConfiguration.ApplicationName + IniExtension, false));
			// Load the fixed settings to a variable, so we can mark these as "fixed"
			_fixedProperties = Read(CreateIniLocation(CoreConfiguration.ApplicationName + FixedPostfix + IniExtension, true));

			// Load our own constants
			var constants = Read(CreateIniLocation(CoreConfiguration.ApplicationName + ConstantsPostfix + IniExtension, true));
			if (constants != null) {
				if (_fixedProperties == null) {
					_fixedProperties = constants;
				} else {
					foreach (string section in constants.Keys) {
						foreach (string property in constants[section].Keys) {
							// Check if section already in fixed properties, if not add it
							if (!_fixedProperties.ContainsKey(section)) {
								_fixedProperties.Add(section, new Dictionary<string, string>());
							}
							// Check if property already in section, if not add if so overwrite!
							if (_fixedProperties[section].ContainsKey(property)) {
								_fixedProperties[section][property] = constants[section][property];
							} else {
								_fixedProperties[section].Add(property, constants[section][property]);
							}
						}
					}
				}
			}

			// Fill all the sections
			foreach (var section in _iniSections) {
				try {
					section.Value.Fill(PropertiesForSection(section.Value.Name));
					FixProperties(section.Value, section.Value.Name);
				} catch (Exception ex) {
					string sectionName = "unknown";
					if (section != null && section.Value.Name != null) {
						sectionName = section.Value.Name;
					}
					LOG.WarnFormat("Problem reading the ini section {0}", sectionName);
					LOG.Warn("Exception", ex);
				}
			}
		}

		/// <summary>
		/// This "fixes" the properties of the section, meaning any properties in the fixed file can't be changed.
		/// </summary>
		/// <param name="section">IniSection</param>
		private void FixProperties(IniSection section, string sectionName) {
			// Make properties unchangable
			if (_fixedProperties != null) {
				Dictionary<string, string> fixedPropertiesForSection;
				if (_fixedProperties.TryGetValue(sectionName, out fixedPropertiesForSection)) {
					foreach (string fixedPropertyKey in fixedPropertiesForSection.Keys) {
						if (section.Values.ContainsKey(fixedPropertyKey)) {
							section.Values[fixedPropertyKey].IsFixed = true;
						}
					}
				}
			}
		}

		/// <summary>
		/// Read the ini file into the Dictionary
		/// </summary>
		/// <param name="iniLocation">Path & Filename of ini file</param>
		private Dictionary<string, Dictionary<string, string>> Read(string iniLocation) {
			if (!File.Exists(iniLocation)) {
				LOG.Info("Can't find file: " + iniLocation);
				return null;
			}
			LOG.InfoFormat("Loading ini-file: {0}", iniLocation);
			//LOG.Info("Reading ini-properties from file: " + iniLocation);
			Dictionary<string, Dictionary<string, string>> newSections = IniReader.read(iniLocation, Encoding.UTF8);
			// Merge the newly loaded properties to the already available
			foreach (string section in newSections.Keys) {
				Dictionary<string, string> newProperties = newSections[section];
				if (!_sections.ContainsKey(section)) {
					// This section is not yet loaded, simply add the complete section
					_sections.Add(section, newProperties);
				} else {
					// Overwrite or add every property from the newly loaded section to the available one
					Dictionary<string, string> currentProperties = _sections[section];
					foreach (string propertyName in newProperties.Keys) {
						string propertyValue = newProperties[propertyName];
						if (currentProperties.ContainsKey(propertyName)) {
							// Override current value as we are loading in a certain order which insures the default, current and fixed
							currentProperties[propertyName] = propertyValue;
						} else {
							// Add "new" value
							currentProperties.Add(propertyName, propertyValue);
						}
					}
				}
			}
			return newSections;
		}

		/// <summary>
		/// Get the raw properties for a section
		/// </summary>
		/// <param name="section"></param>
		/// <returns></returns>
		public Dictionary<string, string> PropertiesForSection(string sectionName) {
			// Get the properties for the section
			Dictionary<string, string> properties;
			if (_sections.ContainsKey(sectionName)) {
				properties = _sections[sectionName];
			} else {
				_sections.Add(sectionName, new Dictionary<string, string>());
				properties = _sections[sectionName];
			}
			return properties;
		}

		/// <summary>
		/// Save the ini file
		/// </summary>
		public void Save() {
			bool acquiredLock = false;
			try {
				acquiredLock = Monitor.TryEnter(IniLock, TimeSpan.FromMilliseconds(200));
				if (acquiredLock) {
					// Code that accesses resources that are protected by the lock.
					string iniLocation = CreateIniLocation(CoreConfiguration.ApplicationName + IniExtension, false);
					try {
						SaveInternally(iniLocation);
					} catch (Exception ex) {
						LOG.Error("A problem occured while writing the configuration file to: " + iniLocation);
						LOG.Error(ex);
					}
				} else {
					// Code to deal with the fact that the lock was not acquired.
					LOG.Warn("A second thread tried to save the ini-file, we blocked as the first took too long.");
				}
			} finally {
				if (acquiredLock) {
					Monitor.Exit(IniLock);
				}
			}
		}

		/// <summary>
		/// The real save implementation
		/// </summary>
		/// <param name="iniLocation"></param>
		private void SaveInternally(string iniLocation) {
			LOG.Info("Saving configuration to: " + iniLocation);
			string iniPath = Path.GetDirectoryName(iniLocation);
			if (iniPath != null && !Directory.Exists(iniPath)) {
				Directory.CreateDirectory(iniPath);
			}
			using (MemoryStream memoryStream = new MemoryStream()) {
				using (TextWriter writer = new StreamWriter(memoryStream, Encoding.UTF8)) {
					ISet<string> writtenSections = new HashSet<string>();
					foreach (var section in _iniSections) {
						try {
							section.Value.Write(writer, false);
							writtenSections.Add(section.Value.Name);
						} catch (Exception ex) {
							LOG.Warn(string.Format("Couldn't write section {0}", section.Value.Name), ex);
						}
						// Add empty line after section
						writer.WriteLine();
						section.Value.IsDirty = false;
					}
					writer.WriteLine();
					// Write left over properties
					foreach (string sectionName in _sections.Keys) {
						// Check if the section is one that is "registered", if so skip it!
						if (!writtenSections.Contains(sectionName)) {
							writer.WriteLine("; The section {0} hasn't been 'claimed' since the last start of Greenshot, therefor it doesn't have additional information here!", sectionName);
							writer.WriteLine("; The reason could be that the section {0} just hasn't been used, a plugin has an error and can't claim it or maybe the whole section {0} is obsolete.", sectionName);
							// Write section name
							writer.WriteLine("[{0}]", sectionName);
							Dictionary<string, string> properties = _sections[sectionName];
							// Loop and write properties
							foreach (string propertyName in properties.Keys) {
								writer.WriteLine("{0}={1}", propertyName, properties[propertyName]);
							}
							writer.WriteLine();
						}
					}
					// Don't forget to flush the buffer
					writer.Flush();
					// Now write the created .ini string to the real file
					using (FileStream fileStream = new FileStream(iniLocation, FileMode.Create, FileAccess.Write)) {
						memoryStream.WriteTo(fileStream);
					}
				}
			}
		}
	}
}
