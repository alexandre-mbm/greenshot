﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Windows.Input;

namespace Greenshot.Modules {
	/// <summary>
	/// The ActionCommand is used at those locations where we want to wrap a method (Action) to a command.
	/// e.g. in the Context-Menu where every item needs an action to be made when we click it.
	/// </summary>
	public class ActionCommand : ICommand {
		private readonly Action<object> _execute;
		private readonly Func<object, bool> _canExecute;
		public event EventHandler CanExecuteChanged {
			add {
				CommandManager.RequerySuggested += value;
			}
			remove {
				CommandManager.RequerySuggested -= value;
			}
		}
		public ActionCommand(Action<object> execute) {
			_execute = execute;
		}
		public ActionCommand(Action<object> execute, Func<object, bool> canExecute) {
			_execute = execute;
			_canExecute = canExecute;
		}
		public void Execute(object parameter) {
			_execute(parameter);
		}
		public bool CanExecute(object parameter) {
			return (_canExecute == null) || _canExecute(parameter);
		}
		public void RaiseCanExecuteChanged() {
			CommandManager.InvalidateRequerySuggested();
		}
	}
}
