﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using log4net;
using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;

namespace Greenshot.Modules {
	public delegate void HotkeyHandler(int hotkeyRegistrationNr);
	/// <summary>
	/// This native window is used to handle all the windows messages, like the hotkeys.
	/// </summary>
	[Export]
	public class NativeGreenshotWindow : NativeWindow, IWin32Window {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(NativeGreenshotWindow));
		private const uint WM_HOTKEY = 0x312;
		public event HotkeyHandler HotkeyEvent;

		/// <summary>
		/// Default constructor, needed to register the handle
		/// </summary>
		public NativeGreenshotWindow() {
			// create the handle for the NativeWindow.
			this.CreateHandle(new System.Windows.Forms.CreateParams());
		}

		/// <summary>
		/// This method is handling the windows events
		/// </summary>
		/// <param name="m"></param>
		protected override void WndProc(ref System.Windows.Forms.Message m) {
			if (m.Msg == WM_HOTKEY) {
				// Call event handlers 
				if (HotkeyEvent != null) {
					try {
						HotkeyEvent((int)m.WParam);
					} catch (Exception ex) {
						LOG.Warn("Exception in Hotkey event handling", ex);
					}
				}
				return;
			}
			base.WndProc(ref m);
		}

		/// <summary>
		/// Make sure the handle is disposed
		/// </summary>
		public void Dispose() {
			this.DestroyHandle();
		}
	}
}
