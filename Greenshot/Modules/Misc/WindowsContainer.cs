﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.ObjectModel;
using Greenshot.Capturing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Greenshot.Modules {
	[Export]
	public class WindowsContainer {
		// Used for caching so we can find the WindowInfo object quickly without iterating
		private readonly IDictionary<IntPtr, WindowInfo> _windowsCache = new ConcurrentDictionary<IntPtr, WindowInfo>();

		// The linked list with all the Top windows
		private readonly ObservableCollection<WindowInfo> _windows = new ObservableCollection<WindowInfo>();
		public ObservableCollection<WindowInfo> Windows {
			get {
				return _windows;
			}
		}

		/// <summary>
		/// Cache to all WindowInfo
		/// </summary>
		public IDictionary<IntPtr, WindowInfo> WindowsCache {
			get {
				return _windowsCache;
			}
		}

		/// <summary>
		/// Get the currently active window
		/// </summary>
		public WindowInfo ActiveWindow {
			get {
				if (_windows.Count > 0) {
					return _windows[0];
				}
				return null;
			}
		}

		/// <summary>
		/// Get the hWnd for the AppLauncer
		/// </summary>
		public WindowInfo AppLauncher {
			get {
				IntPtr appLauncherHwnd = WindowInfo.AppLauncher;

				WindowInfo appLauncher = null;
				if (_windowsCache.TryGetValue(appLauncherHwnd, out appLauncher)) {
					appLauncher = WindowInfo.CreateFor(appLauncherHwnd);
					_windowsCache.Add(appLauncherHwnd, appLauncher);
				}
				return appLauncher;
			}
		}

	}
}
