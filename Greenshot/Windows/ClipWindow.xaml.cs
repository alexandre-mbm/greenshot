﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Greenshot.Helpers;
using GreenshotPlugin.Modules;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Greenshot.Windows {
	/// <summary>
	/// Interaction logic for ClipWindow.xaml
	/// </summary>
	[Export]
	public partial class ClipWindow : Window, INotifyPropertyChanged {
		private EventDelay delayMouse = new EventDelay(TimeSpan.FromMilliseconds(15).Ticks);
		private EventDelay delayOutput = new EventDelay(TimeSpan.FromMilliseconds(1000).Ticks);
		private double x1, y1, x2, y2;
		private bool isLeft = false;
		private bool isTop = false;
		private Point position = new Point(int.MinValue, int.MinValue);
		public event PropertyChangedEventHandler PropertyChanged;
		bool isMouseDown = false;
		/// <summary>
		/// This method is called by the Set accessor of each property. 
		/// The CallerMemberName attribute that is applied to the optional propertyName 
		/// parameter causes the property name of the caller to be substituted as an argument. 
		/// </summary>
		/// <param name="propertyName"></param>
		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "") {
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		private double _zoom = 5;
		public double Zoom {
			get {
				return _zoom;
			}
			set {
				_zoom = value;
				NotifyPropertyChanged();
			}
		}
		private double _px = int.MinValue;
		public double Px {
			get {
				return _px;
			}
			set {
				_px = value;
				NotifyPropertyChanged();
			}
		}
		private double _py = int.MinValue;
		public double Py {
			get {
				return _py;
			}
			set {
				_py = value;
				NotifyPropertyChanged();
			}
		}

		private double _magnifierX = int.MinValue;
		public double MagnifierX {
			get {
				return _magnifierX;
			}
			set {
				_magnifierX = value;
				NotifyPropertyChanged();
			}
		}
		private double _magnifierY = int.MinValue;
		public double MagnifierY {
			get {
				return _magnifierY;
			}
			set {
				_magnifierY = value;
				NotifyPropertyChanged();
			}
		}

		public CaptureContext CaptureContext {
			get;
			set;
		}

		public ClipWindow() {
			InitializeComponent();
			DataContext = this;
			PreviewKeyDown += new KeyEventHandler(HandleKeys);
			BaseCanvas.MouseMove += Canvas_MouseMove;
		}

		/// <summary>
		/// Start animations
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Window_Loaded(object sender, RoutedEventArgs e) {
			var horizontalStoryboard = FindResource("MouseRight") as Storyboard;
			Storyboard.SetTarget(horizontalStoryboard, Magnifier);
			horizontalStoryboard.Begin(Magnifier, true);

			var verticalStoryboard = FindResource("MouseBottom") as Storyboard;
			Storyboard.SetTarget(verticalStoryboard, Magnifier);
			verticalStoryboard.Begin(Magnifier, true);
		}

		private void HandleKeys(object sender, KeyEventArgs e) {
			if (e.Key == Key.Escape) {
				Close();
			}
		}

		private void Canvas_MouseMove(object sender, MouseEventArgs e) {
			if (!delayMouse.Check()) {
				return;
			}
			if (delayOutput.Check()) {
				Console.WriteLine(string.Format("Coordinates: {0},{1}", position.X, position.Y));
			}
			// Get the x and y coordinates of the mouse pointer.
			position = e.GetPosition(this);
			if (position.X == int.MinValue) {
				return;
			}

			Px = position.X;
			Py = position.Y;

			// Center Magnifier on the Mouse-Location
			MagnifierX = Px - ((30 * Zoom) / 2);
			MagnifierY = Py - ((30 * Zoom) / 2);

			// Calculate if we need to move (animate) the magnifier to a different location
			double magnifierSize = (30 * Zoom) + 100;
			Storyboard horizontalStoryboard = null;
			if (isLeft && Px < magnifierSize) {
				horizontalStoryboard = FindResource("MouseRight") as Storyboard;
				isLeft = false;
			}
			if (!isLeft && Px > (BaseCanvas.ActualWidth - magnifierSize)) {
				horizontalStoryboard = FindResource("MouseLeft") as Storyboard;
				isLeft = true;
			}
			if (horizontalStoryboard != null) {
				Console.WriteLine("Changing horizontal");
				Storyboard.SetTarget(horizontalStoryboard, Magnifier);
				horizontalStoryboard.Begin(Magnifier, true);
			}

			Storyboard verticalStoryboard = null;
			if (isTop && Py < magnifierSize) {
				verticalStoryboard = FindResource("MouseBottom") as Storyboard;
				isTop = false;
			}
			if (!isTop && Py > (BaseCanvas.ActualHeight - magnifierSize)) {
				verticalStoryboard = FindResource("MouseTop") as Storyboard;
				isTop = true;
			}
			//  && verticalStoryboard.GetCurrentState() != ClockState.Active
			if (verticalStoryboard != null) {
				Console.WriteLine("Changing Vertical");
				Storyboard.SetTarget(verticalStoryboard, Magnifier);
				verticalStoryboard.Begin(Magnifier, true);
			}


			// Draw selected
			if (isMouseDown) {
				Canvas.SetLeft(SelectedArea, Math.Min(x1, Px));
				Canvas.SetTop(SelectedArea, Math.Min(y1, Py));
				SelectedArea.Width = Math.Abs(Px - x1);
				SelectedArea.Height = Math.Abs(Py - y1);
			}
			// Set the "cross-hair"
			CrossHorizontal1.X1 = 0;
			CrossHorizontal1.Y1 = Py;
			CrossHorizontal1.X2 = Px - 10;
			CrossHorizontal1.Y2 = Py;
			CrossHorizontal2.X1 = Px + 10;
			CrossHorizontal2.Y1 = Py;
			CrossHorizontal2.X2 = BaseCanvas.ActualWidth;
			CrossHorizontal2.Y2 = Py;

			CrossVertical1.X1 = Px;
			CrossVertical1.Y1 = 0;
			CrossVertical1.X2 = Px;
			CrossVertical1.Y2 = Py - 10;
			CrossVertical2.X1 = Px;
			CrossVertical2.Y1 = Py + 10;
			CrossVertical2.X2 = Px;
			CrossVertical2.Y2 = BaseCanvas.ActualHeight;

			// Sets the position of the image to the mouse coordinates.
			MagnifiedContent.Viewbox = new Rect(new Point(Px - (30 / 2), Py - (30 / 2)), new Size(30, 30));
		}

		private void Canvas_MouseDown(object sender, MouseButtonEventArgs e) {
			isMouseDown = true;
			System.Windows.Point position = e.GetPosition(this);
			x1 = position.X;
			y1 = position.Y;
			SelectedArea.Visibility = Visibility.Visible;
		}

		private void Canvas_MouseUp(object sender, MouseButtonEventArgs e) {
			isMouseDown = false;
			System.Windows.Point position = e.GetPosition(this);
			x2 = position.X;
			y2 = position.Y;

			Rect cropRectange = new Rect((int)Math.Min(x1, x2), (int)Math.Min(y1, y2), (int)Math.Abs(x1 - x2), (int)Math.Abs(y1 - y2));
			if (cropRectange.Width * cropRectange.Height > 0) {
				// Save
				CaptureContext.ClipArea = new RectangleGeometry(cropRectange);
				CaptureContext.CropRect = cropRectange;
			}
			Close();
		}

		private void MyCanvas_MouseWheel(object sender, MouseWheelEventArgs e) {
			if (e.Delta > 0) {
				Zoom = Zoom + 0.4;
			} else if (e.Delta < 0) {
				Zoom = Zoom - 0.4;
			}
		}

		private void Storyboard_Completed(object sender, EventArgs e) {
			string storyBoardName = ((ClockGroup)sender).Timeline.Name;
			Console.WriteLine(storyBoardName);
			//var storyboard = FindResource(storyBoardName) as Storyboard;
			//CopyAnimatedValuesToLocalValues(Magnifier);
			//storyboard.Remove();
		}

		private void CopyAnimatedValuesToLocalValues(DependencyObject obj) {
			// Recurse down tree
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++) {
				CopyAnimatedValuesToLocalValues(VisualTreeHelper.GetChild(obj, i));
			}

			var enumerator = obj.GetLocalValueEnumerator();
			while (enumerator.MoveNext()) {
				var prop = enumerator.Current.Property;
				var value = enumerator.Current.Value as Freezable;

				// Recurse into eg. brushes that may be set by storyboard, as long as they aren't frozen
				if (value != null && !value.IsFrozen) {
					CopyAnimatedValuesToLocalValues(value);
				}

				// *** This is the key bit of code ***
				if (DependencyPropertyHelper.GetValueSource(obj, prop).IsAnimated) {
					obj.SetValue(prop, obj.GetValue(prop));
				}

			}
		}
	}
}
