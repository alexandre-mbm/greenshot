﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Windows;
using GreenshotPlugin.Modules;
using System.Collections.ObjectModel;
using Greenshot.Modules;
using System.Linq;
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Windows.Media;

namespace Greenshot.Windows {
	/// <summary>
	/// Interaction logic for DestinationPickerWindow.xaml
	/// </summary>
	[Export]
	public partial class DestinationPickerWindow : Window {
		public CaptureContext CaptureContext {
			get;
			set;
		}
		[ImportMany]
		private IEnumerable<Lazy<IDestination, IDestinationMetadata>> _destinations = null;

		[Import("SvgImages")]
		public ObservableDictionary<string, DrawingImage> SvgImages {
			get;
			set;
		}

		public IEnumerable<Lazy<IDestination, IDestinationMetadata>> Destinations {
			get {
				return from destination in _destinations where !"DestinationPicker".Equals(destination.Metadata.Designation) && destination.Metadata.IconFilename != null select destination;
			}
		}

		public DestinationPickerWindow(CaptureContext captureContext) {
			ModuleContainer.InjectThis(this);
			CaptureContext = captureContext;
			InitializeComponent();
			this.DataContext = this;
		}
	}
}
