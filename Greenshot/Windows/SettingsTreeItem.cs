﻿using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Greenshot.Windows {
	public class SettingsTreeItem : INotifyPropertyChanged {
		private readonly IDictionary<string, SettingsTreeItem> _children = new SortedDictionary<string, SettingsTreeItem>();
		public event PropertyChangedEventHandler PropertyChanged;
		private bool _visible = true;
		private bool _marked;
		private bool _expanded;

		public void ResetUIProperties(bool fullReset) {
			Visible = true;
			Marked = false;
			if (fullReset) {
				Expanded = false;
			}
		}

		public bool Visible {
			get {
				return _visible;
			}
			set {
				_visible = value;
				if (PropertyChanged != null) {
					PropertyChanged(this, new PropertyChangedEventArgs("Visible"));
				}
			}
		}

		public bool Marked {
			get {
				return _marked;
			}
			set {
				_marked = value;
				if (PropertyChanged != null) {
					PropertyChanged(this, new PropertyChangedEventArgs("Marked"));
				}
			}
		}

		public bool Expanded {
			get {
				return _expanded;
			}
			set {
				_expanded = value;
				if (PropertyChanged != null) {
					PropertyChanged(this, new PropertyChangedEventArgs("Expanded"));
				}
			}
		}

		public String Key {
			get;
			set;
		}

		public String Text {
			get {
				return Language.GetString(Key);
			}
		}

		public SettingsPage SettingsPage {
			get;
			set;
		}

		public IDictionary<string, SettingsTreeItem> Children {
			get {
				return _children;
			}
		}

		/// <summary>
		/// Make sure the Children are ordered how we like them
		/// </summary
		public ObservableCollection<SettingsTreeItem> ChildrenSortedByText {
			get {
				return new ObservableCollection<SettingsTreeItem>(_children.Values.OrderBy(i => i.Text));
			}
		}
	}
}
