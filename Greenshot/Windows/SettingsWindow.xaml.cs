﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Greenshot.Windows {
	/// <summary>
	/// The SettingsWindow is the window which contains the treeview with all the settings pages and show the currently selected settings page.
	/// </summary>
	[Export(typeof(SettingsWindow))]
	public partial class SettingsWindow : Window, IPartImportsSatisfiedNotification {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(SettingsWindow));
		private readonly IDictionary<string, SettingsTreeItem> _treeItems = new SortedDictionary<string, SettingsTreeItem>();
		private readonly string _initialSelectedPath;

		[Import]
		public IDictionary<string, IniProxy> IniSections {
			get;
			set;
		}

		[ImportMany]
		private IEnumerable<Lazy<SettingsPage, ISettingsPageMetadata>> SettingsPages {
			get;
			set;
		}

		[Import("SvgImages")]
		public ObservableDictionary<string, DrawingImage> SvgImages {
			get;
			set;
		}

		#region Code to register the settings pages

		private SettingsTreeItem FindSettingsTreeItem(string keyPath) {
			IDictionary<string, SettingsTreeItem> current = _treeItems;
			string[] pathEntries = keyPath.Split(new[] { ',' });
			for (int i = 0; i < pathEntries.Length; i++) {
				string currentLocation = pathEntries[i];
				if (current.ContainsKey(currentLocation)) {
					if (i == pathEntries.Length - 1) {
						return current[currentLocation];
					}
					current = current[currentLocation].Children;
				}
			}
			return null;
		}

		/// <summary>
		/// Register the SettingsPage Type T or URL to the supplied the keyPath
		/// </summary>
		/// <param name="keyPath">key,key,key</param>
		/// <param name="settingsPage"></param>
		private void RegisterSettingsPage(string keyPath, SettingsPage settingsPage) {
			IDictionary<string, SettingsTreeItem> current = _treeItems;
			string[] pathEntries = keyPath.Split(new[] { ',' });
			for (int i = 0; i < pathEntries.Length; i++ ) {
				string currentLocation = pathEntries[i];
				if (current.ContainsKey(currentLocation)) {
					if (i == pathEntries.Length - 1) {
						current[currentLocation].SettingsPage = settingsPage;
					}
					current = current[currentLocation].Children;
				} else if (i < pathEntries.Length - 1) {
					var item = new SettingsTreeItem {
						Key = currentLocation
					};
					current.Add(currentLocation, item);
					current = item.Children;
				} else {
					current.Add(currentLocation, new SettingsTreeItem {
						Key = currentLocation,
						SettingsPage = settingsPage,
					});
				}
			}
		}

		#endregion

		#region Public Properties
		/// <summary>
		/// Make sure the elements in the SettingsTree are ordered how we like them
		/// </summary>
		public ObservableCollection<SettingsTreeItem> SettingsTree {
			get {
				return new ObservableCollection<SettingsTreeItem>(_treeItems.Values.OrderBy(i => i.Text));
			}
		}

		#endregion
		/// <summary>
		/// Constructor, selecting the default path
		/// </summary>
		public SettingsWindow() {
			Loaded += SettingsWindow_Loaded;
			Closing += Window_Closing;
			_initialSelectedPath = "settings_general";
		}

		/// <summary>
		/// Make the window reusable
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Window_Closing(object sender, CancelEventArgs e) {
			e.Cancel = true;
			Visibility = Visibility.Hidden;
		}

		public void SettingsWindow_Loaded(object sender, RoutedEventArgs e) {
			if (_initialSelectedPath != null) {
				SetSelectedItem(FindSettingsTreeItem(_initialSelectedPath));
			}
			ResetUi(_treeItems, true);
			InitializeComponent();
			DataContext = this;
		}

		#region Event handlers
		/// <summary>
		/// The user wants to apply the changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OK_Click(object sender, RoutedEventArgs e) {
			foreach (var proxy in IniSections.Values) {
				proxy.Commit();
			}
			foreach (var settingsPage in SettingsPages) {
				settingsPage.Value.Commit();
			}
			// TODO: Save??
			//IniConfig.Save();
			Close();
		}

		/// <summary>
		/// The user doesn't want to apply the changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Cancel_Click(object sender, RoutedEventArgs e) {
			foreach (var proxy in IniSections.Values) {
				proxy.Rollback();
			}
			foreach (var settingsPage in SettingsPages) {
				settingsPage.Value.Rollback();
			}
			Close();
		}

		private void Clear_Click(object sender, RoutedEventArgs e) {
			FilterBox.Text = null;
			ApplyFilter();
		}

		/// <summary>
		/// An element in the Settings Tree is selected, now do:
		/// 1) if a SettingsPage is registered to the item: 
		/// a) Create the page if it wasn't created
		/// b) Show the page
		/// 2) if a URL is registered to the item, navigate to it
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
			SettingsTreeItem settingsTreeItem = (SettingsTreeItem)e.NewValue;
			if (settingsTreeItem != null) {
				CurrentSettingsPage.Content = settingsTreeItem.SettingsPage;
			}
		}

		/// <summary>
		/// Key up on the Textbox with the filter, make all elements which don't match invisible
		/// Make the elements that match highlighted
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Filter_KeyUp(object sender, KeyEventArgs e) {
			ApplyFilter();
		}

		#endregion

		#region Helper methods

		/// <summary>
		/// Apply the current filter to the tree items
		/// </summary>
		private void ApplyFilter() {
			string filterText = FilterBox.Text;
			if (!string.IsNullOrEmpty(filterText) && filterText.Length >= 2) {
				filterText = filterText.ToLower();
				Filter(_treeItems, filterText);
			} else {
				ResetUi(_treeItems, false);
			}
		}

		/// <summary>
		/// Reset the elements in the UI
		/// </summary>
		/// <param name="treeItemsToApply"></param>
		private static void ResetUi(IDictionary<string, SettingsTreeItem> treeItemsToApply, bool fullReset) {
			if (treeItemsToApply == null) {
				return;
			}
			foreach (SettingsTreeItem settingsTreeItem in treeItemsToApply.Values) {
				settingsTreeItem.ResetUIProperties(fullReset);
				ResetUi(settingsTreeItem.Children, fullReset);
			}
		}

		/// <summary>
		/// Apply the filter to all the SettingsTreeItems
		/// </summary>
		/// <param name="treeItemsToApply"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		private bool Filter(IDictionary<string, SettingsTreeItem> treeItemsToApply, string filter) {
			if (treeItemsToApply == null) {
				return false;
			}
			bool foundElements = false;
			foreach (SettingsTreeItem settingsTreeItem in treeItemsToApply.Values) {
				bool foundChildren = Filter(settingsTreeItem.Children, filter);
				if (foundChildren) {
					foundElements = true;
				}

				// check tree item name
				if (settingsTreeItem.Text.ToLower().Contains(filter)) {
					settingsTreeItem.Visible = true;
					settingsTreeItem.Expanded = true;
					settingsTreeItem.Marked = !string.IsNullOrEmpty(filter);
					foundElements = true;
					continue;
				}
				if (settingsTreeItem.SettingsPage != null) {
					// check page content
					Page settingsTreeItemPage = settingsTreeItem.SettingsPage;
					if (HasText(settingsTreeItemPage, filter)) {
						settingsTreeItem.Visible = true;
						settingsTreeItem.Expanded = true;
						settingsTreeItem.Marked = !string.IsNullOrEmpty(filter);
						foundElements = true;
						continue;
					}
				}

				settingsTreeItem.Marked = false;
				settingsTreeItem.Expanded = foundChildren;
				settingsTreeItem.Visible = foundChildren;
			}
			return foundElements;
		}

		/// <summary>
		/// Walks the tree items to find the node corresponding with
		/// the given item, then sets it to be selected.
		/// </summary>
		/// <param name="item">The item to be selected</param>
		/// <returns><c>true</c> if the item was found and set to be
		/// selected</returns>
		public bool SetSelectedItem(object item) {
			return SetSelected(SettingsTreeView, item);
		}

		static private bool SetSelected(ItemsControl parent, object child) {
			if (parent == null || child == null) {
				return false;
			}

			var childNode = parent.ItemContainerGenerator.ContainerFromItem(child) as TreeViewItem;

			if (childNode != null) {
				childNode.Focus();
				return childNode.IsSelected = true;
			}

			if (parent.Items.Count > 0) {
				foreach (object childItem in parent.Items) {
					var childControl = parent.ItemContainerGenerator.ContainerFromItem(childItem) as ItemsControl;

					if (SetSelected(childControl, child)) {
						return true;
					}
				}
			}

			return false;
		}

		private static bool HasText(object current, string text) {
			// The logical tree can contain any type of object, not just 
			// instances of DependencyObject subclasses.  LogicalTreeHelper
			// only works with DependencyObject subclasses, so we must be
			// sure that we do not pass it an object of the wrong type.
			var dependencyObject = current as DependencyObject;
			if (dependencyObject != null) {
				foreach (object logicalChild in LogicalTreeHelper.GetChildren(dependencyObject)) {
					if (logicalChild is ContentControl) {
						var control = logicalChild as ContentControl;
						if (control.Content is string) {
							string content = control.Content as string;
							if (!string.IsNullOrEmpty(content)) {
								if (content.ToLower().Contains(text)) {
									//control.Background = System.Windows.Media.Brushes.Red;
									return true;
								}
							}
						}
					}
					if (HasText(logicalChild, text)) {
						return true;
					}
				}
			}
			return false;
		}
		#endregion

		/// <summary>
		/// Register the pages as soon as all imports are set, this is called before the SettingsWindow_Loaded and prevents a slow startup
		/// </summary>
		public void OnImportsSatisfied() {
			foreach (var settingsPage in SettingsPages) {
				try {
					LOG.InfoFormat("Adding page to {0}", settingsPage.Metadata.Path);
					RegisterSettingsPage(settingsPage.Metadata.Path, settingsPage.Value);
				} catch (Exception ex) {
					LOG.Error("Error added settings page: ", ex);
				}
			}
		}
	}
}
