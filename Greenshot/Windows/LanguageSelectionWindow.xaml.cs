﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Greenshot.Windows {
	/// <summary>
	/// Make it possible for the user to select a language
	/// </summary>
	[Export]
	public partial class LanguageSelectionWindow : Window, IPartImportsSatisfiedNotification {
		[Import("SvgImages")]
		public ObservableDictionary<string, DrawingImage> SvgImages {
			get;
			set;
		}

		public IList<LanguageFile> Languages {
			get {
				return GreenshotPlugin.Core.Language.SupportedLanguages;
			}
		}

		public LanguageFile SelectedLanguage {
			get;
			set;
		}

		public LanguageSelectionWindow() {
			string ietf = Thread.CurrentThread.CurrentUICulture.Name;
			if (GreenshotPlugin.Core.Language.CurrentLanguage != null) {
				ietf = GreenshotPlugin.Core.Language.CurrentLanguage;
			}
			SelectedLanguage = GreenshotPlugin.Core.Language.SupportedLanguages[0];

			foreach (var languageFile in GreenshotPlugin.Core.Language.SupportedLanguages) {
				if (languageFile.Ietf.StartsWith(ietf)) {
					SelectedLanguage = languageFile;
					break;
				}
			}

		}

		private void Window_Closed(object sender, System.EventArgs e) {
			 GreenshotPlugin.Core.Language.CurrentLanguage = SelectedLanguage.Ietf;
		}

		public void OnImportsSatisfied() {
			DataContext = this;
			InitializeComponent();
		}
	}
}
