﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Greenshot.Interop;
using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Imaging;
using GreenshotPlugin.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Greenshot.Capturing {
	/// <summary>
	/// This class holds all the available information on a Window.
	/// The information is updated (or reset) by the WinEventHook
	/// </summary>
	public class WindowInfo : INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;

		#region App Store Windows
		public const string ModernuiWindowsClass = "Windows.UI.Core.CoreWindow";
		public const string ModernuiApplauncherClass = "ImmersiveLauncher";
		public const string ModernuiGutterClass = "ImmersiveGutter";
		// All currently known classes: "ImmersiveGutter", "Snapped Desktop", "ImmersiveBackgroundWindow","ImmersiveLauncher","Windows.UI.Core.CoreWindow","ApplicationManager_ImmersiveShellWindow","SearchPane","MetroGhostWindow","EdgeUiInputWndClass", "NativeHWNDHost", "Shell_CharmWindow"
		private static readonly IAppVisibility AppVisibility;
		// For MonitorFromWindow
		public const int MONITOR_DEFAULTTONULL = 0;
		public const int MONITOR_DEFAULTTOPRIMARY = 1;
		public const int MONITOR_DEFAULTTONEAREST = 2;

		// This is used for Windows 8 to see if the App Launcher is active
		// See http://msdn.microsoft.com/en-us/library/windows/desktop/jj554119%28v=vs.85%29.aspx
		[ComProgId("clsid:7E5FE3D9-985F-4908-91F9-EE19F9FD1514")]
		[ComImport, Guid("2246EA2D-CAEA-4444-A3C4-6DE827E44313"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IAppVisibility {
			MONITOR_APP_VISIBILITY GetAppVisibilityOnMonitor(IntPtr hMonitor);
			bool IsLauncherVisible {
				get;
			}
		}

		private enum MONITOR_APP_VISIBILITY {
			MAV_UNKNOWN = 0,		// The mode for the monitor is unknown
			MAV_NO_APP_VISIBLE = 1,
			MAV_APP_VISIBLE = 2
		}

		static WindowInfo() {
			try {
				// Only try to instanciate when Windows 8 or later.
				if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 2) {
					AppVisibility = COMWrapper.CreateInstance<IAppVisibility>();					
				}
			} catch {
			}
		}

		/// <summary>
		/// Check if a Windows Store ap (WinRT) is visible
		/// </summary>
		/// <param name="windowBounds"></param>
		/// <returns>true if the app, covering the supplied rect, is visisble</returns>
		private static bool AppVisible(Rect windowBounds) {
			foreach (var screen in System.Windows.Forms.Screen.AllScreens) {
				if (screen.Bounds.ToRect().Contains(windowBounds)) {
					if (windowBounds.Equals(screen.Bounds)) {
						// Fullscreen, it's "visible" when AppVisibilityOnMonitor says yes
						// Although it might be the other App, this is not "very" important
						RECT rect = new RECT(screen.Bounds.ToRect());
						IntPtr monitor = MonitorFromRect(ref rect, MONITOR_DEFAULTTONULL);
						if (monitor != IntPtr.Zero && AppVisibility != null) {
							MONITOR_APP_VISIBILITY monitorAppVisibility = AppVisibility.GetAppVisibilityOnMonitor(monitor);
							//LOG.DebugFormat("App {0} visible: {1} on {2}", Text, monitorAppVisibility, screen.Bounds);
							if (monitorAppVisibility == MONITOR_APP_VISIBILITY.MAV_APP_VISIBLE) {
								return true;
							}
						}
					} else {
						// Is only partly on the screen, when this happens the app is allways visible!
						return true;
					}
				}
			}
			return false;
		}
		/// <summary>
		/// Return true if the app-launcher is visible
		/// </summary>
		private static bool IsLauncherVisible {
			get {
				if (AppVisibility != null) {
					return AppVisibility.IsLauncherVisible;
				}
				return false;
			}
		}
		/// <summary>
		/// Get the hWnd for the AppLauncer
		/// </summary>
		public static IntPtr AppLauncher {
			get {
				if (AppVisibility == null) {
					return IntPtr.Zero;
				}
				return FindWindow(ModernuiApplauncherClass, null);
			}
		}

		/// <summary>
		/// Retrieve all Metro apps
		/// </summary>
		private static IEnumerable<WindowInfo> MetroApps {
			get {
				// if the appVisibility != null we have Windows 8.
				if (AppVisibility == null) {
					yield break;
				}
				IntPtr nextHandle = FindWindow(ModernuiWindowsClass, null);
				while (nextHandle != IntPtr.Zero) {
					yield return WindowInfo.CreateFor(nextHandle);
					nextHandle = FindWindowEx(IntPtr.Zero, nextHandle, ModernuiWindowsClass, null);
				}
				// check for gutter
				IntPtr gutterHandle = FindWindow(ModernuiGutterClass, null);
				if (gutterHandle != IntPtr.Zero) {
					yield return WindowInfo.CreateFor(gutterHandle);
				}
			}
		}

		#endregion

		#region Native imports
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetParent(IntPtr hWnd);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetWindowText(IntPtr hWnd, StringBuilder lpString, int cch);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
		[DllImport("user32", SetLastError = true)]
		private extern static IntPtr SendMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLong(IntPtr hWnd, int nIndex);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLongPtr(IntPtr hWnd, int nIndex);
		[DllImport("user32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private extern static bool IsZoomed(IntPtr hwnd);
		[DllImport("user32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private extern static bool IsWindowVisible(IntPtr hWnd);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr MonitorFromRect([In] ref RECT lprc, uint dwFlags);
		[DllImport("user32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
		[DllImport("dwmapi", SetLastError = true)]
		private static extern int DwmIsCompositionEnabled(out bool enabled);
		[DllImport("dwmapi", SetLastError = true)]
		private static extern int DwmGetWindowAttribute(IntPtr hwnd, int dwAttribute, out RECT lpRect, int size);
		#endregion

		#region native structures
		[StructLayout(LayoutKind.Sequential), Serializable()]
		private struct RECT {
			private int _Left;
			private int _Top;
			private int _Right;
			private int _Bottom;

			public RECT(RECT rectangle)
				: this(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom) {
			}
			public RECT(Rect rectangle)
				: this((int)rectangle.Left, (int)rectangle.Top, (int)rectangle.Right, (int)rectangle.Bottom) {
			}
			public RECT(int left, int top, int right, int bottom) {
				_Left = left;
				_Top = top;
				_Right = right;
				_Bottom = bottom;
			}

			public int X {
				get {
					return _Left;
				}
				set {
					_Left = value;
				}
			}
			public int Y {
				get {
					return _Top;
				}
				set {
					_Top = value;
				}
			}
			public int Left {
				get {
					return _Left;
				}
				set {
					_Left = value;
				}
			}
			public int Top {
				get {
					return _Top;
				}
				set {
					_Top = value;
				}
			}
			public int Right {
				get {
					return _Right;
				}
				set {
					_Right = value;
				}
			}
			public int Bottom {
				get {
					return _Bottom;
				}
				set {
					_Bottom = value;
				}
			}
			public int Height {
				get {
					return unchecked(_Bottom - _Top);
				}
				set {
					_Bottom = unchecked(value - _Top);
				}
			}
			public int Width {
				get {
					return unchecked(_Right - _Left);
				}
				set {
					_Right = unchecked(value + _Left);
				}
			}
			public Point Location {
				get {
					return new Point(Left, Top);
				}
				set {
					_Left = (int)value.X;
					_Top = (int)value.Y;
				}
			}
			public Size Size {
				get {
					return new Size(Width, Height);
				}
				set {
					_Right = unchecked((int)value.Width + _Left);
					_Bottom = unchecked((int)value.Height + _Top);
				}
			}

			public static implicit operator Rect(RECT rectangle) {
				return new Rect(rectangle.Left, rectangle.Top, rectangle.Width, rectangle.Height);
			}
			public static implicit operator RECT(Rect rectangle) {
				return new RECT((int)rectangle.Left, (int)rectangle.Top, (int)rectangle.Right, (int)rectangle.Bottom);
			}
			public static bool operator ==(RECT rectangle1, RECT rectangle2) {
				return rectangle1.Equals(rectangle2);
			}
			public static bool operator !=(RECT rectangle1, RECT rectangle2) {
				return !rectangle1.Equals(rectangle2);
			}

			public override string ToString() {
				return "{Left: " + _Left + "; " + "Top: " + _Top + "; Right: " + _Right + "; Bottom: " + _Bottom + "}";
			}

			public override int GetHashCode() {
				return ToString().GetHashCode();
			}

			public bool Equals(RECT rectangle) {
				return rectangle.Left == _Left && rectangle.Top == _Top && rectangle.Right == _Right && rectangle.Bottom == _Bottom;
			}

			public Rect ToRect() {
				return new Rect(Left, Top, Width, Height);
			}
			public override bool Equals(object Object) {
				if (Object is RECT) {
					return Equals((RECT)Object);
				}
				if (Object is Rect) {
					return Equals(new RECT((Rect)Object));
				}

				return false;
			}
		}

		/// <summary>
		/// The structure for the WindowInfo
		/// See: http://msdn.microsoft.com/en-us/library/windows/desktop/ms632610%28v=vs.85%29.aspx
		/// </summary>
		[StructLayout(LayoutKind.Sequential), Serializable]
		private struct WINDOWINFO {
			public uint cbSize;
			public RECT rcWindow;
			public RECT rcClient;
			public uint dwStyle;
			public uint dwExStyle;
			public uint dwWindowStatus;
			public uint cxWindowBorders;
			public uint cyWindowBorders;
			public ushort atomWindowType;
			public ushort wCreatorVersion;
			// Allows automatic initialization of "cbSize" with "new WINDOWINFO(null/true/false)".
			public WINDOWINFO(Boolean? filler)
				: this() {
				cbSize = (UInt32)(Marshal.SizeOf(typeof(WINDOWINFO)));
			}
		}
		#endregion

		#region Native enums
		/// <summary>
		/// See http://msdn.microsoft.com/en-us/library/aa969530(v=vs.85).aspx
		/// </summary>
		private enum DWMWINDOWATTRIBUTE {
			DWMWA_NCRENDERING_ENABLED = 1,
			DWMWA_NCRENDERING_POLICY,
			DWMWA_TRANSITIONS_FORCEDISABLED,
			DWMWA_ALLOW_NCPAINT,
			DWMWA_CAPTION_BUTTON_BOUNDS,
			DWMWA_NONCLIENT_RTL_LAYOUT,
			DWMWA_FORCE_ICONIC_REPRESENTATION,
			DWMWA_FLIP3D_POLICY,
			DWMWA_EXTENDED_FRAME_BOUNDS, // This is the one we need for retrieving the Window size since Windows Vista
			DWMWA_HAS_ICONIC_BITMAP,        // Since Windows 7
			DWMWA_DISALLOW_PEEK,            // Since Windows 7
			DWMWA_EXCLUDED_FROM_PEEK,       // Since Windows 7
			DWMWA_CLOAK,                    // Since Windows 8
			DWMWA_CLOAKED,                  // Since Windows 8
			DWMWA_FREEZE_REPRESENTATION,    // Since Windows 8
			DWMWA_LAST
		}

		/// <summary>
		/// A Win32 error code.
		/// </summary>
		private enum Win32Error : uint {
			Success = 0x0,
			InvalidFunction = 0x1,
			FileNotFound = 0x2,
			PathNotFound = 0x3,
			TooManyOpenFiles = 0x4,
			AccessDenied = 0x5,
			InvalidHandle = 0x6,
			ArenaTrashed = 0x7,
			NotEnoughMemory = 0x8,
			InvalidBlock = 0x9,
			BadEnvironment = 0xa,
			BadFormat = 0xb,
			InvalidAccess = 0xc,
			InvalidData = 0xd,
			OutOfMemory = 0xe,
			InvalidDrive = 0xf,
			CurrentDirectory = 0x10,
			NotSameDevice = 0x11,
			NoMoreFiles = 0x12,
			WriteProtect = 0x13,
			BadUnit = 0x14,
			NotReady = 0x15,
			BadCommand = 0x16,
			Crc = 0x17,
			BadLength = 0x18,
			Seek = 0x19,
			NotDosDisk = 0x1a,
			SectorNotFound = 0x1b,
			OutOfPaper = 0x1c,
			WriteFault = 0x1d,
			ReadFault = 0x1e,
			GenFailure = 0x1f,
			SharingViolation = 0x20,
			LockViolation = 0x21,
			WrongDisk = 0x22,
			SharingBufferExceeded = 0x24,
			HandleEof = 0x26,
			HandleDiskFull = 0x27,
			NotSupported = 0x32,
			RemNotList = 0x33,
			DupName = 0x34,
			BadNetPath = 0x35,
			NetworkBusy = 0x36,
			DevNotExist = 0x37,
			TooManyCmds = 0x38,
			FileExists = 0x50,
			CannotMake = 0x52,
			AlreadyAssigned = 0x55,
			InvalidPassword = 0x56,
			InvalidParameter = 0x57,
			NetWriteFault = 0x58,
			NoProcSlots = 0x59,
			TooManySemaphores = 0x64,
			ExclSemAlreadyOwned = 0x65,
			SemIsSet = 0x66,
			TooManySemRequests = 0x67,
			InvalidAtInterruptTime = 0x68,
			SemOwnerDied = 0x69,
			SemUserLimit = 0x6a
		}

		private enum WindowsMessages : int {
			WM_NULL = 0x0000,
			WM_CREATE = 0x0001,
			WM_DESTROY = 0x0002,
			WM_MOVE = 0x0003,
			WM_SIZE = 0x0005,
			WM_ACTIVATE = 0x0006,
			WM_SETFOCUS = 0x0007,
			WM_KILLFOCUS = 0x0008,
			WM_ENABLE = 0x000A,
			WM_SETREDRAW = 0x000B,
			WM_SETTEXT = 0x000C,
			WM_GETTEXT = 0x000D,
			WM_GETTEXTLENGTH = 0x000E,
			WM_PAINT = 0x000F,
			WM_CLOSE = 0x0010,
			WM_QUERYENDSESSION = 0x0011,
			WM_QUIT = 0x0012,
			WM_QUERYOPEN = 0x0013,
			WM_ERASEBKGND = 0x0014,
			WM_SYSCOLORCHANGE = 0x0015,
			WM_ENDSESSION = 0x0016,
			WM_SHOWWINDOW = 0x0018,
			WM_WININICHANGE = 0x001A,
			WM_SETTINGCHANGE = 0x001A,
			WM_DEVMODECHANGE = 0x001B,
			WM_ACTIVATEAPP = 0x001C,
			WM_FONTCHANGE = 0x001D,
			WM_TIMECHANGE = 0x001E,
			WM_CANCELMODE = 0x001F,
			WM_SETCURSOR = 0x0020,
			WM_MOUSEACTIVATE = 0x0021,
			WM_CHILDACTIVATE = 0x0022,
			WM_QUEUESYNC = 0x0023,
			WM_GETMINMAXINFO = 0x0024,
			WM_PAINTICON = 0x0026,
			WM_ICONERASEBKGND = 0x0027,
			WM_NEXTDLGCTL = 0x0028,
			WM_SPOOLERSTATUS = 0x002A,
			WM_DRAWITEM = 0x002B,
			WM_MEASUREITEM = 0x002C,
			WM_DELETEITEM = 0x002D,
			WM_VKEYTOITEM = 0x002E,
			WM_CHARTOITEM = 0x002F,
			WM_SETFONT = 0x0030,
			WM_GETFONT = 0x0031,
			WM_SETHOTKEY = 0x0032,
			WM_GETHOTKEY = 0x0033,
			WM_QUERYDRAGICON = 0x0037,
			WM_COMPAREITEM = 0x0039,
			WM_GETOBJECT = 0x003D,
			WM_COMPACTING = 0x0041,
			WM_COMMNOTIFY = 0x0044,
			WM_WINDOWPOSCHANGING = 0x0046,
			WM_WINDOWPOSCHANGED = 0x0047,
			WM_POWER = 0x0048,
			WM_COPYDATA = 0x004A,
			WM_CANCELJOURNAL = 0x004B,
			WM_NOTIFY = 0x004E,
			WM_INPUTLANGCHANGEREQUEST = 0x0050,
			WM_INPUTLANGCHANGE = 0x0051,
			WM_TCARD = 0x0052,
			WM_HELP = 0x0053,
			WM_USERCHANGED = 0x0054,
			WM_NOTIFYFORMAT = 0x0055,
			WM_CONTEXTMENU = 0x007B,
			WM_STYLECHANGING = 0x007C,
			WM_STYLECHANGED = 0x007D,
			WM_DISPLAYCHANGE = 0x007E,
			WM_GETICON = 0x007F,
			WM_SETICON = 0x0080,
			WM_NCCREATE = 0x0081,
			WM_NCDESTROY = 0x0082,
			WM_NCCALCSIZE = 0x0083,
			WM_NCHITTEST = 0x0084,
			WM_NCPAINT = 0x0085,
			WM_NCACTIVATE = 0x0086,
			WM_GETDLGCODE = 0x0087,
			WM_SYNCPAINT = 0x0088,
			WM_NCMOUSEMOVE = 0x00A0,
			WM_NCLBUTTONDOWN = 0x00A1,
			WM_NCLBUTTONUP = 0x00A2,
			WM_NCLBUTTONDBLCLK = 0x00A3,
			WM_NCRBUTTONDOWN = 0x00A4,
			WM_NCRBUTTONUP = 0x00A5,
			WM_NCRBUTTONDBLCLK = 0x00A6,
			WM_NCMBUTTONDOWN = 0x00A7,
			WM_NCMBUTTONUP = 0x00A8,
			WM_NCMBUTTONDBLCLK = 0x00A9,
			WM_KEYFIRST = 0x0100,
			WM_KEYDOWN = 0x0100,
			WM_KEYUP = 0x0101,
			WM_CHAR = 0x0102,
			WM_DEADCHAR = 0x0103,
			WM_SYSKEYDOWN = 0x0104,
			WM_SYSKEYUP = 0x0105,
			WM_SYSCHAR = 0x0106,
			WM_SYSDEADCHAR = 0x0107,
			WM_KEYLAST = 0x0108,
			WM_IME_STARTCOMPOSITION = 0x010D,
			WM_IME_ENDCOMPOSITION = 0x010E,
			WM_IME_COMPOSITION = 0x010F,
			WM_IME_KEYLAST = 0x010F,
			WM_INITDIALOG = 0x0110,
			WM_COMMAND = 0x0111,
			WM_SYSCOMMAND = 0x0112,
			WM_TIMER = 0x0113,
			WM_HSCROLL = 0x0114,
			WM_VSCROLL = 0x0115,
			WM_INITMENU = 0x0116,
			WM_INITMENUPOPUP = 0x0117,
			WM_MENUSELECT = 0x011F,
			WM_MENUCHAR = 0x0120,
			WM_ENTERIDLE = 0x0121,
			WM_MENURBUTTONUP = 0x0122,
			WM_MENUDRAG = 0x0123,
			WM_MENUGETOBJECT = 0x0124,
			WM_UNINITMENUPOPUP = 0x0125,
			WM_MENUCOMMAND = 0x0126,
			WM_CTLCOLORMSGBOX = 0x0132,
			WM_CTLCOLOREDIT = 0x0133,
			WM_CTLCOLORLISTBOX = 0x0134,
			WM_CTLCOLORBTN = 0x0135,
			WM_CTLCOLORDLG = 0x0136,
			WM_CTLCOLORSCROLLBAR = 0x0137,
			WM_CTLCOLORSTATIC = 0x0138,
			WM_MOUSEMOVE = 0x0200,
			WM_MOUSEFIRST = 0x0200,
			WM_LBUTTONDOWN = 0x0201,
			WM_LBUTTONUP = 0x0202,
			WM_LBUTTONDBLCLK = 0x0203,
			WM_RBUTTONDOWN = 0x0204,
			WM_RBUTTONUP = 0x0205,
			WM_RBUTTONDBLCLK = 0x0206,
			WM_MBUTTONDOWN = 0x0207,
			WM_MBUTTONUP = 0x0208,
			WM_MBUTTONDBLCLK = 0x0209,
			WM_MOUSEWHEEL = 0x020A,
			WM_MOUSELAST = 0x020A,
			WM_PARENTNOTIFY = 0x0210,
			WM_ENTERMENULOOP = 0x0211,
			WM_EXITMENULOOP = 0x0212,
			WM_SIZING = 0x0214,
			WM_CAPTURECHANGED = 0x0215,
			WM_MOVING = 0x0216,
			WM_POWERBROADCAST = 0x0218,
			WM_DEVICECHANGE = 0x0219,
			WM_MDICREATE = 0x0220,
			WM_MDIDESTROY = 0x0221,
			WM_MDIACTIVATE = 0x0222,
			WM_MDIRESTORE = 0x0223,
			WM_MDINEXT = 0x0224,
			WM_MDIMAXIMIZE = 0x0225,
			WM_MDITILE = 0x0226,
			WM_MDICASCADE = 0x0227,
			WM_MDIICONARRANGE = 0x0228,
			WM_MDIGETACTIVE = 0x0229,
			WM_MDISETMENU = 0x0230,
			WM_ENTERSIZEMOVE = 0x0231,
			WM_EXITSIZEMOVE = 0x0232,
			WM_DROPFILES = 0x0233,
			WM_MDIREFRESHMENU = 0x0234,
			WM_IME_SETCONTEXT = 0x0281,
			WM_IME_NOTIFY = 0x0282,
			WM_IME_CONTROL = 0x0283,
			WM_IME_COMPOSITIONFULL = 0x0284,
			WM_IME_SELECT = 0x0285,
			WM_IME_CHAR = 0x0286,
			WM_IME_REQUEST = 0x0288,
			WM_IME_KEYDOWN = 0x0290,
			WM_IME_KEYUP = 0x0291,
			WM_NCMOUSEHOVER = 0x02A0,
			WM_MOUSEHOVER = 0x02A1,
			WM_NCMOUSELEAVE = 0x02A2,
			WM_MOUSELEAVE = 0x02A3,
			WM_CUT = 0x0300,
			WM_COPY = 0x0301,
			WM_PASTE = 0x0302,
			WM_CLEAR = 0x0303,
			WM_UNDO = 0x0304,
			WM_RENDERFORMAT = 0x0305,
			WM_RENDERALLFORMATS = 0x0306,
			WM_DESTROYCLIPBOARD = 0x0307,
			WM_DRAWCLIPBOARD = 0x0308,
			WM_PAINTCLIPBOARD = 0x0309,
			WM_VSCROLLCLIPBOARD = 0x030A,
			WM_SIZECLIPBOARD = 0x030B,
			WM_ASKCBFORMATNAME = 0x030C,
			WM_CHANGECBCHAIN = 0x030D,
			WM_HSCROLLCLIPBOARD = 0x030E,
			WM_QUERYNEWPALETTE = 0x030F,
			WM_PALETTEISCHANGING = 0x0310,
			WM_PALETTECHANGED = 0x0311,
			WM_HOTKEY = 0x0312,
			WM_PRINT = 0x0317,
			WM_PRINTCLIENT = 0x0318,
			WM_HANDHELDFIRST = 0x0358,
			WM_HANDHELDLAST = 0x035F,
			WM_AFXFIRST = 0x0360,
			WM_AFXLAST = 0x037F,
			WM_PENWINFIRST = 0x0380,
			WM_PENWINLAST = 0x038F,
			WM_APP = 0x8000,
			WM_USER = 0x0400
		}

		private enum ClassLongIndex : int {
			GCL_CBCLSEXTRA = -20, // the size, in bytes, of the extra memory associated with the class. Setting this value does not change the number of extra bytes already allocated.
			GCL_CBWNDEXTRA = -18, // the size, in bytes, of the extra window memory associated with each window in the class. Setting this value does not change the number of extra bytes already allocated. For information on how to access this memory, see SetWindowLong.
			GCL_HBRBACKGROUND = -10, // a handle to the background brush associated with the class.
			GCL_HCURSOR = -12, // a handle to the cursor associated with the class.
			GCL_HICON = -14, // a handle to the icon associated with the class.
			GCL_HICONSM = -34, // a handle to the small icon associated with the class.
			GCL_HMODULE = -16, // a handle to the module that registered the class.
			GCL_MENUNAME = -8, // the address of the menu name string. The string identifies the menu resource associated with the class.
			GCL_STYLE = -26, // the window-class style bits.
			GCL_WNDPROC = -24, // the address of the window procedure, or a handle representing the address of the window procedure. You must use the CallWindowProc function to call the window procedure. 
		}
		#endregion

		#region Native wrapper methods
		private static Win32Error GetLastErrorCode() {
			return (Win32Error)Marshal.GetLastWin32Error();
		}

		/// <summary>
		/// Helper method to get the window size for GDI Windows
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="rectangle">out Rectangle</param>
		/// <returns>bool true if it worked</returns>		
		private static bool GetWindowRect(IntPtr hWnd, out Rect rectangle) {
			WINDOWINFO windowInfo = new WINDOWINFO();
			// Get the Window Info for this window
			bool result = GetWindowInfo(hWnd, ref windowInfo);
			if (result) {
				rectangle = windowInfo.rcWindow.ToRect();
			} else {
				rectangle = Rect.Empty;
			}
			return result;
		}

		/// <summary>
		/// Helper method to get the window size for GDI Windows
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="rectangle">out Rectangle</param>
		/// <returns>bool true if it worked</returns>		
		private static bool GetClientRect(IntPtr hWnd, out Rect rectangle) {
			WINDOWINFO windowInfo = new WINDOWINFO();
			// Get the Window Info for this window
			bool result = GetWindowInfo(hWnd, ref windowInfo);
			if (result) {
				rectangle = windowInfo.rcClient.ToRect();
			} else {
				rectangle = Rect.Empty;
			}
			return result;
		}

		/// <summary>
		/// Helper method to get the Border size for GDI Windows
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="size"></param>
		/// <returns>bool true if it worked</returns>	
		private static bool GetBorderSize(IntPtr hWnd, out Size size) {
			WINDOWINFO windowInfo = new WINDOWINFO();
			// Get the Window Info for this window
			bool result = GetWindowInfo(hWnd, ref windowInfo);
			if (result) {
				size = new Size((int)windowInfo.cxWindowBorders, (int)windowInfo.cyWindowBorders);
			} else {
				size = Size.Empty;
			}
			return result;
		}

		/// <summary>
		/// Wrapper for the GetClassLong which decides if the system is 64-bit or not and calls the right one.
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <param name="nIndex">int</param>
		/// <returns>IntPtr</returns>
		private static IntPtr GetClassLongWrapper(IntPtr hWnd, int nIndex) {
			if (IntPtr.Size > 4) {
				return GetClassLongPtr(hWnd, nIndex);
			}
			return GetClassLong(hWnd, nIndex);
		}

		/// <summary>
		/// Helper method for an easy DWM check
		/// </summary>
		/// <returns>bool true if DWM is available AND active</returns>
		private static bool IsDwmEnabled {
			get {
				// According to: http://technet.microsoft.com/en-us/subscriptions/aa969538%28v=vs.85%29.aspx
				// And: http://msdn.microsoft.com/en-us/library/windows/desktop/aa969510%28v=vs.85%29.aspx
				// DMW is always enabled on Windows 8! So return true and save a check! ;-)
				if (Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor == 2) {
					return true;
				}
				if (Environment.OSVersion.Version.Major >= 6) {
					bool dwmEnabled;
					DwmIsCompositionEnabled(out dwmEnabled);
					return dwmEnabled;
				}
				return false;
			}
		}

		/// <summary>
		/// Helper method to get the window size for DWM Windows
		/// </summary>
		/// <param name="rectangle">out Rectangle</param>
		/// <returns>bool true if it worked</returns>
		private static bool GetExtendedFrameBounds(IntPtr hWnd, out Rect rectangle) {
			RECT rect;
			int result = DwmGetWindowAttribute(hWnd, (int)DWMWINDOWATTRIBUTE.DWMWA_EXTENDED_FRAME_BOUNDS, out rect, Marshal.SizeOf(typeof(RECT)));
			if (result >= 0) {
				rectangle = rect.ToRect();
				return true;
			}
			rectangle = Rect.Empty;
			return false;
		}

		/// <summary>
		/// Retrieve the windows title, also called Text
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string GetText(IntPtr hWnd) {
			StringBuilder title = new StringBuilder(260, 260);
			int length = GetWindowText(hWnd, title, title.Capacity);
			if (length == 0) {
				Win32Error error = GetLastErrorCode();
				if (error != Win32Error.Success) {
					return null;
				}
			}
			return title.ToString();
		}
		/// <summary>
		/// Retrieve the windows classname
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string GetClassname(IntPtr hWnd) {
			StringBuilder classNameBuilder = new StringBuilder(260, 260);
			int hresult = GetClassName(hWnd, classNameBuilder, classNameBuilder.Capacity);
			if (hresult == 0) {
				return null;
			}
			return classNameBuilder.ToString();
		}


		/// <summary>
		/// Get the icon for a hWnd
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		private static ImageSource GetIcon(IntPtr hWnd) {
			IntPtr iconSmall = IntPtr.Zero;
			IntPtr iconBig = new IntPtr(1);
			IntPtr iconSmall2 = new IntPtr(2);

			IntPtr iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconSmall2, IntPtr.Zero);
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconSmall, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICONSM);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconBig, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICON);
			}

			if (iconHandle == IntPtr.Zero) {
				return null;
			}

			using (var icon = System.Drawing.Icon.FromHandle(iconHandle)) {
				return icon.ToImageSource();
			}
		}

		#endregion
		/// <summary>
		/// A helper method to create the WindowInfo for a hWnd with it's parent
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <returns>WindowInfo</returns>
		public static WindowInfo CreateFor(IntPtr hWnd) {
			return new WindowInfo {
				Handle = hWnd,
				Classname = GetClassname(hWnd),
				Parent = GetParent(hWnd),
				Bounds = Rect.Empty
			};
		}

		/// <summary>
		/// Equals override
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(Object obj) {
			// Check for null values and compare run-time types.
			if (obj == null || GetType() != obj.GetType())
				return false;

			WindowInfo wi = (WindowInfo)obj;
			return wi.Handle == Handle;
		}

		/// <summary>
		/// GetHashCode override
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			return Handle.GetHashCode();
		}

		/// <summary>
		/// The hWnd
		/// </summary>
		public IntPtr Handle {
			get;
			set;
		}

		/// <summary>
		/// The parent hWnd
		/// </summary>
		public IntPtr Parent {
			get;
			set;
		}

		/// <summary>
		/// Returns true if this WindowInfo has a parent
		/// </summary>
		public bool HasParent {
			get {
				return IntPtr.Zero != Parent;
			}
		}

		private readonly IList<WindowInfo> children = new List<WindowInfo>();
		/// <summary>
		/// List of children
		/// </summary>
		public IList<WindowInfo> Children {
			get {
				return children;
			}
		}


		private string _text;
		/// <summary>
		/// Property for the "title" of the window, use "Property = null" to update
		/// </summary>
		public string Text {
			get {
				if (_text == null) {
					_text = GetText(Handle);
				}
				return _text;
			}
			set {
				_text = value;
				PropertyChanged.Notify(() => Text);
			}
		}

		private string _classname;
		/// <summary>
		/// Classname of this window
		/// </summary>
		public string Classname {
			get {
				if (_classname == null) {
					_classname = GetClassname(Handle);
				}
				return _classname;
			}
			set {
				_classname = value;
				PropertyChanged.Notify(() => Classname);
			}
		}

		/// <summary>
		/// A simple check if this window has a classname, this skips any retrieval
		/// </summary>
		public bool HasClassname {
			get {
				return _classname != null;
			}
		}

		private Rect _bounds;

		/// <summary>
		/// Bounds for this window
		/// </summary>
		public Rect Bounds {
			get {
				return _bounds;
			}
			set {
				if (!value.IsEmpty) {
					_bounds = value;
				} else {
					var windowRect = Rect.Empty;
					if (!HasParent && IsDwmEnabled) {
						GetExtendedFrameBounds(Handle, out windowRect);
					}

					if (windowRect.IsEmpty) {
						GetWindowRect(Handle, out windowRect);
					}

					// Correction for maximized windows, only if it's not an app
					if (!HasParent && !IsApp && Maximised) {
						Size size;
						GetBorderSize(Handle, out size);
						windowRect = new Rect(windowRect.X + size.Width, windowRect.Y + size.Height, windowRect.Width - (2 * size.Width), windowRect.Height - (2 * size.Height));
					}
					_bounds = windowRect;
				}
				PropertyChanged.Notify(() => Bounds);
			}
		}

		/// <summary>
		/// Checks if this window is maximized
		/// </summary>
		public bool Maximised {
			get {
				if (IsApp) {
					if (Visible) {
						foreach (var screen in System.Windows.Forms.Screen.AllScreens) {
							if (_bounds.Equals(screen.Bounds)) {
								return true;
							}
						}
					}
					return false;
				}
				return IsZoomed(Handle);
			}
		}

		/// <summary>
		/// Gets whether the window is visible.
		/// </summary>
		public bool Visible {
			get {
				if (IsApp) {
					return AppVisible(Bounds);
				}
				if (IsGutter) {
					// gutter is only made available when it's visible
					return true;
				}
				if (IsAppLauncher) {
					return IsLauncherVisible;
				}
				return IsWindowVisible(Handle);
			}
		}

		private ImageSource _displayIcon;
		/// <summary>
		/// Returns an imagesource with the icon for this window
		/// </summary>
		public ImageSource DisplayIcon {
			get {
				if (_displayIcon == null) {
					_displayIcon = GetIcon(Handle);
				}
				return _displayIcon;
			}
			set {
				_displayIcon = value;
			}
		}

		/// <summary>
		/// Returns true if this window is an App
		/// </summary>
		public bool IsApp {
			get {
				return ModernuiWindowsClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app "gutter"
		/// </summary>
		public bool IsGutter {
			get {
				return ModernuiGutterClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app launcher (Windows 8 start screen)
		/// </summary>
		public bool IsAppLauncher {
			get {
				return ModernuiApplauncherClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Check if this window is the window of a metro app
		/// </summary>
		public bool IsMetroApp {
			get {
				return IsAppLauncher || IsApp;
			}
		}
	}
}
