﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Greenshot.Composition {
	/// <summary>
	/// This makes sure we don't get problems loading plugins, see this stackoverflow article
	/// http://stackoverflow.com/questions/4144683/handle-reflectiontypeloadexception-during-mef-composition
	/// </summary>
	public class SafeDirectoryCatalog : ComposablePartCatalog {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(SafeDirectoryCatalog));
		private readonly AggregateCatalog _catalog;

		public SafeDirectoryCatalog(string directory)
			: this(directory, "*.dll") {
		}
		public SafeDirectoryCatalog(string directory, string pattern) {
			var files = Directory.EnumerateFiles(directory, pattern, SearchOption.AllDirectories);

			_catalog = new AggregateCatalog();

			foreach (var file in files) {
				try {
					var asmCat = new AssemblyCatalog(file);

					//Force MEF to load the plugin and figure out if there are any exports
					// good assemblies will not throw an exception and can be added to the catalog
					if (asmCat.Parts.ToList().Count > 0) {
						_catalog.Catalogs.Add(asmCat);
					}
				} catch (Exception ex) {
					LOG.ErrorFormat(string.Format("Error loading {0}", file), ex);
				}
			}
		}

		public override IQueryable<ComposablePartDefinition> Parts {
			get {
				return _catalog.Parts;
			}
		}
	}
}
