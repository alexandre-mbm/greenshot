
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin.WPF;
using System.ComponentModel;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using System.Windows.Markup;

namespace Greenshot.Composition {
	[ContentProperty("Catalog")]
	public class DesigntimeBootstrapper : CompositionBootstrapper, ISupportInitialize {
		private readonly bool _inDesignMode;

		/// <summary>
		/// Gets or sets the design-time catalog.
		/// </summary>
		public DesigntimeCatalog Catalog {
			get;
			set;
		}

		public DesigntimeBootstrapper() {
			_inDesignMode = DesignerProperties.GetIsInDesignMode(new DependencyObject());

			if (_inDesignMode) {
				CompositionProperties.SetBootstrapper(Application.Current, this);
			}
		}

		/// <summary>
		/// Use the Catalog added at design time.
		/// </summary>
		protected override void ConfigureAggregateCatalog() {
			if (Catalog != null) {
				AggregateCatalog.Catalogs.Add(Catalog);
			}
		}

		void ISupportInitialize.BeginInit() {
		}

		void ISupportInitialize.EndInit() {
			if (_inDesignMode) {
				Run();
			}
		}
	}
}
