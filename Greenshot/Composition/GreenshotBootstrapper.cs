/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Windows;
using System.ComponentModel.Composition;

namespace Greenshot.Composition {
	/// <summary>
	/// Code comes from http://www.codeproject.com/Articles/155802/Blendability-Part-IV-Design-Time-Support-for-MEF
	/// </summary>
	public class GreenshotBootstrapper : RuntimeBootstrapper {
		private ISet<string> _pluginPaths = new HashSet<string>();

		public GreenshotBootstrapper(Application application) : base(application) {
		}

		protected override void ConfigureAggregateCatalog() {
			base.ConfigureAggregateCatalog();
			// Composition parts can be defined inside greenshot itself
			var entryAssembly = Assembly.GetExecutingAssembly();
			AggregateCatalog.Catalogs.Add(new AssemblyCatalog(entryAssembly));
			var installationDirectory = Path.GetDirectoryName(entryAssembly.Location);

			// Composition parts in the plugin dll (this is where the CoreConfiguration is located)
			var pluginAssembly = Assembly.GetAssembly(typeof(CoreConfiguration));
			AggregateCatalog.Catalogs.Add(new AssemblyCatalog(pluginAssembly));

			// Composition parts in the editor dll (this is currently not a plugin)
			AggregateCatalog.Catalogs.Add(new SafeDirectoryCatalog(installationDirectory, "GreenshotEditor.dll"));

			// TODO: make it possible to load plugins directory from the local projects, instead of having postbuild scripts
			//if (CoreConfiguration.IsDebugOrInDesignMode) {
				// Code to add loading of the local plugin projects
			//}
			string pafPath = Path.Combine(installationDirectory, @"App\Greenshot\Plugins");

			// The rest
			if (Directory.Exists(pafPath)) {
				// Portable
				foreach (string pluginFile in Directory.GetFiles(pafPath, "*.gsp", SearchOption.AllDirectories)) {
					var pluginPath = Path.GetDirectoryName(pluginFile);
					_pluginPaths.Add(pluginPath);
					AggregateCatalog.Catalogs.Add(new SafeDirectoryCatalog(pluginPath, "*.gsp"));
				}
			} else {
				// Plugin path
				var pluginsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), CoreConfiguration.ApplicationName + @"\Plugins");

				if (Directory.Exists(pluginsPath)) {
					foreach (string pluginFile in Directory.GetFiles(pluginsPath, "*.gsp", SearchOption.AllDirectories)) {
						var pluginPath = Path.GetDirectoryName(pluginFile);
						_pluginPaths.Add(pluginPath);
						AggregateCatalog.Catalogs.Add(new SafeDirectoryCatalog(pluginPath, "*.gsp"));
					}
				}

				var applicationPath = Path.Combine(installationDirectory, @"Plugins");

				if (Directory.Exists(applicationPath)) {
					foreach (string pluginFile in Directory.GetFiles(applicationPath, "*.gsp", SearchOption.AllDirectories)) {
						var pluginPath = Path.GetDirectoryName(pluginFile);
						_pluginPaths.Add(pluginPath);
						AggregateCatalog.Catalogs.Add(new SafeDirectoryCatalog(pluginPath, "*.gsp"));
					}
				}
			}
		}

		protected override void ConfigureContainer() {
			base.ConfigureContainer();
			// Make sure the Container is available to code outside of the Greenshot project
			ModuleContainer.MefContainer = Container;
			// Export all found plugin locations
			Container.ComposeExportedValue<ISet<string>>("PluginPaths", _pluginPaths);
		}
	}
}
