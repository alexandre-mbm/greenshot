﻿using System.ComponentModel.Composition.Primitives;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Linq;

namespace Greenshot.Composition {
	public abstract class DesigntimeCatalogAdapter<TCatalog> : DesigntimeCatalog
		where TCatalog : ComposablePartCatalog {
		private bool _disposed;
		private TCatalog _innerCatalog;

		protected TCatalog InnerCatalog {
			get {
				if (_innerCatalog == null) {
					this._innerCatalog = CreateInnerCatalog();
				}

				return _innerCatalog;
			}
		}

		public override IQueryable<ComposablePartDefinition> Parts {
			get {
				return InnerCatalog.Parts;
			}
		}

		protected override void Dispose(bool disposing) {
			if (_disposed) {
				return;
			}

			if (disposing) {
				InnerCatalog.Dispose();
				_innerCatalog = null;
			}

			_disposed = true;
			base.Dispose(disposing);
		}

		protected abstract TCatalog CreateInnerCatalog();
	}
}
