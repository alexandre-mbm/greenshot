﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin;
using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the DestinationPickerSettingsPage.xaml
	/// </summary>
	[SettingsPage(Path = "settings_destination,settings_destination_picker")]
	public partial class DestinationPickerSettingsPage : SettingsPage {
		private readonly ObservableCollection<ILegacyDestination> availableDestinations = new ObservableCollection<ILegacyDestination>();
		private readonly ObservableCollection<ILegacyDestination> selectedDestinations = new ObservableCollection<ILegacyDestination>();

		public ObservableCollection<ILegacyDestination> AvailableDestinations {
			get {
				return availableDestinations;
			}
		}

		public ObservableCollection<ILegacyDestination> SelectedDestinations {
			get {
				return selectedDestinations;
			}
		}

		[Import]
		private CoreConfiguration _coreConfiguration = null;

		protected override void Initialize() {
			base.Initialize();
			InitializeComponent();

			if (_coreConfiguration.PickerDestinations == null) {
				_coreConfiguration.PickerDestinations = new List<string>();
			}

			if (_coreConfiguration.PickerDestinations.Count > 0) {
				// Show selected (and active) destinations
				foreach (string designation in _coreConfiguration.PickerDestinations) {
					ILegacyDestination destination = DestinationHelper.GetDestination(designation);
					if ("Picker".Equals(destination.Designation)) {
						continue;
					}
					selectedDestinations.Add(destination);
				}
				foreach (ILegacyDestination destination in DestinationHelper.GetAllDestinations()) {
					// Skip picker
					if ("Picker".Equals(destination.Designation)) {
						continue;
					}
					if (!_coreConfiguration.PickerDestinations.Contains(destination.Designation)) {
						availableDestinations.Add(destination);
					}
				}
			} else {
				foreach (ILegacyDestination destination in DestinationHelper.GetAllDestinations()) {
					// Skip picker
					if ("Picker".Equals(destination.Designation)) {
						continue;
					}
					selectedDestinations.Add(destination);
				}
			}

		}

		public override void Commit() {
			base.Commit();
			_coreConfiguration.PickerDestinations.Clear();
			// Are any unselected, only than we need to specify the list
			if (availableDestinations.Count > 0) {
				foreach (ILegacyDestination destination in selectedDestinations) {
					if (!_coreConfiguration.PickerDestinations.Contains(destination.Designation)) {
						_coreConfiguration.PickerDestinations.Add(destination.Designation);
					}
				}
			}
		}
	}
}
