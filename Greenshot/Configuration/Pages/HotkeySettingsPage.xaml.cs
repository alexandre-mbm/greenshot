﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel.Composition;
using System.Windows;
using System.Collections.Generic;
using log4net;
using GreenshotPlugin.Modules;
using GreenshotPlugin.Core.Settings;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the HotkeySettingsPage.xaml
	/// </summary>
	[SettingsPage(Path = "hotkeys")]
	public partial class HotkeySettingsPage : SettingsPage {
		private static ILog LOG = LogManager.GetLogger(typeof(HotkeySettingsPage));

		/// <summary>
		/// All the methods that have [Export("HotkeyFunction")] and are of type "bool Methodname()"
		/// </summary>
		[ImportMany(typeof(IHotkey))]
		public IEnumerable<Lazy<Action, IHotkeyMetadata>> HotkeyExports {
			get;
			set;
		}

		protected override void Initialize() {
			base.Initialize();
			InitializeComponent();
		}

		private void SettingsPage_GotFocus(object sender, RoutedEventArgs e) {
			//SettingsHotkeyTextBox.UnregisterHotkeys();
		}

		private void SettingsPage_LostFocus(object sender, RoutedEventArgs e) {
			// TOdo unregister hotkeys, this should be changed into "temporarily" not accepting hotkeys
			//MainForm.RegisterHotkeys();
		}
	}
}
