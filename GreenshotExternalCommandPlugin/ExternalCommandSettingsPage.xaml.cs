﻿
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows;

namespace ExternalCommand {
	/// <summary>
	/// Interaction logic for ExternalCommandSettingsPage.xaml
	/// </summary>
	[SettingsPage(Path = "settings_plugins,externalcommand.settings_title")]
	public partial class ExternalCommandSettingsPage : SettingsPage {
		[Import]
		private ExternalCommandConfiguration _externalCommandConfiguration = null;

		private readonly ObservableCollection<EditableObjectProxy<ExternalCommandData>> externalCommands = new ObservableCollection<EditableObjectProxy<ExternalCommandData>>();
		public ObservableCollection<EditableObjectProxy<ExternalCommandData>> ExternalCommands {
			get {
				return externalCommands;
			}
		}
		protected override void Initialize() {
			base.Initialize();
			InitializeComponent();
			externalCommands.Clear();
			foreach (ExternalCommandData exCommand in _externalCommandConfiguration.ExternalCommands) {
				var proxy = new EditableObjectProxy<ExternalCommandData>(exCommand);
				proxy.StartTransaction();
				externalCommands.Add(proxy);
			}
		}

		public override void Rollback() {
			base.Rollback();
			foreach (EditableObjectProxy<ExternalCommandData> exCommand in externalCommands) {
				exCommand.Rollback();
			}
		}
		public override void Commit() {
			base.Commit();
			_externalCommandConfiguration.ExternalCommands.Clear();
			foreach (EditableObjectProxy<ExternalCommandData> exCommand in externalCommands) {
				exCommand.Commit();
				// Add current state to the configuration
				_externalCommandConfiguration.ExternalCommands.Add(exCommand.ProxiedObject);
			}
		}

		/// <summary>
		/// Add a new default command to the list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Add_Click(object sender, RoutedEventArgs e) {
			var newCommand = new ExternalCommandData {
				Name = "new"
			};
			var newCommandProxy = new EditableObjectProxy<ExternalCommandData>(newCommand);
			newCommandProxy.StartTransaction();
			externalCommands.Add(newCommandProxy);
		}

		/// <summary>
		/// Remove the current selected item from the list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Delete_Click(object sender, RoutedEventArgs e) {
			if (CommandsList.SelectedIndex >= 0) {
				externalCommands.RemoveAt(CommandsList.SelectedIndex);
			}
		}
	}
}