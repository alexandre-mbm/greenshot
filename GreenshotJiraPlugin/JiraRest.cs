﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using GreenshotPlugin.Core;

namespace GreenshotJiraPlugin {
	/// <summary>
	/// A simple wrapper for JIRA access
	/// </summary>
	public class JiraRest {
		private readonly HttpClient client;
		private string credentials;

		public bool CanConnect {
			get;
			private set;
		}

		public User CurrentUser {
			get;
			private set;
		}

		/// <summary>
		/// Create the jira object, here the HttpClient is configured for usage with a proxy and to use the JIRA rest api
		/// </summary>
		/// <param name="user"></param>
		/// <param name="password"></param>
		public JiraRest(string baseurl, string user, string password) {
			credentials = Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(string.Format("{0}:{1}", user, password)));
			// Initialize the HTTP-Client
			IWebProxy proxy;
			try {
				proxy = WebRequest.GetSystemWebProxy();
			} catch {
				proxy = WebRequest.DefaultWebProxy;
			}
			bool isProxyUsed = proxy != null;
			if (isProxyUsed) {
				proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
			}

			var handler = new HttpClientHandler {
				Proxy = proxy,
				UseProxy = isProxyUsed,
				CookieContainer = new CookieContainer(),
				UseDefaultCredentials = true
			};

			client = new HttpClient(handler) {
				BaseAddress = new Uri(baseurl)
			};
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
			client.DefaultRequestHeaders.TryAddWithoutValidation("X-Atlassian-Token", "nocheck");
			client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Greenshot");

			try {
				CurrentUser = UserDetails(user).Result;
				if (CurrentUser != null) {
					CanConnect = true;
				}
			} catch (AggregateException aEx) {
				throw aEx.InnerException;
			}
		}

		/// <summary>
		/// find issues by jql
		/// </summary>
		/// <param name="jql"></param>
		/// <returns></returns>
		public Task<SearchResult> Find(string jql) {
			return client.GetAsync(string.Format("search?jql={0}", jql)).ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				string jsonResult = responseTask.Result.Content.ReadAsStringAsync().Result;
				var searchResult = JSONSerializer.Deserialize<SearchResult>(jsonResult);
				return searchResult;
			});
		}

		/// <summary>
		/// Get user information
		/// </summary>
		/// <param name="username"></param>
		/// <returns>User in a Task</returns>
		public Task<User> UserDetails(string username) {
			return client.GetAsync(string.Format("user?username={0}", username)).ContinueWith((responseTask) => {
				// Special Handling, this is the first call after the constructor and can cause problems with the password
				if (responseTask.Result.StatusCode == HttpStatusCode.Unauthorized) {
					return null;
				}
				responseTask.Result.EnsureSuccessStatusCode();
				return JSONSerializer.Deserialize<User>(responseTask.Result.Content.ReadAsStringAsync().Result);
			});
		}

		/// <summary>
		/// Get the list of available Projects
		/// </summary>
		/// <returns></returns>
		public Task<List<Project>> Projects() {
			return client.GetAsync("project").ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				return JSONSerializer.Deserialize<List<Project>>(responseTask.Result.Content.ReadAsStringAsync().Result);
			});
		}

		/// <summary>
		/// Get the list of available issue types
		/// </summary>
		/// <returns></returns>
		public Task<List<IssueType>> IssueTypes() {
			return client.GetAsync("issuetype").ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				return JSONSerializer.Deserialize<List<IssueType>>(responseTask.Result.Content.ReadAsStringAsync().Result);
			});
		}

		/// <summary>
		/// Get the list of available components for a project
		/// </summary>
		/// <returns></returns>
		public Task<List<Component>> Components(string projectKey) {
			return client.GetAsync(string.Format("project/{0}/components", projectKey)).ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				return JSONSerializer.Deserialize<List<Component>>(responseTask.Result.Content.ReadAsStringAsync().Result);
			});
		}

		/// <summary>
		/// Get the list of filter favourites
		/// </summary>
		/// <returns></returns>
		public Task<List<FilterFavourite>> Filters() {
			return client.GetAsync("filter/favourite").ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				return JSONSerializer.Deserialize<List<FilterFavourite>>(responseTask.Result.Content.ReadAsStringAsync().Result);
			});
		}

		/// <summary>
		/// Get the issue by key
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Task<Issue> Details(string key) {
			return client.GetAsync(string.Format("issue/{0}", key)).ContinueWith((responseTask) => {
				responseTask.Result.EnsureSuccessStatusCode();
				string jsonResponse = responseTask.Result.Content.ReadAsStringAsync().Result;
				return JSONSerializer.Deserialize<Issue>(jsonResponse);
			});
		}

		/// <summary>
		/// Delete an attachment
		/// </summary>
		/// <param name="key"></param>
		/// <param name="comment"></param>
		/// <returns></returns>
		public Task<bool> DeleteAttachment(string id) {
			return client.DeleteAsync(string.Format("attachment/{0}", id)).ContinueWith(responsetask => {
				responsetask.Result.EnsureSuccessStatusCode();
				return true;
			});
		}

		/// <summary>
		/// Create an issue
		/// </summary>
		/// <param name="newIssue">Issue</param>
		/// <returns>Issue</returns>
		public Task<Issue> Create(Issue newIssue) {
			string jsonRequestData = JSONSerializer.Serialize(newIssue);
			var stringContent = new StringContent(jsonRequestData, Encoding.UTF8, "application/json");
			return client.PostAsync("issue", stringContent).ContinueWith(responseTask => {
				string jsonResponse = responseTask.Result.Content.ReadAsStringAsync().Result;
				if (responseTask.Result.StatusCode == HttpStatusCode.Created) {
					Issue createdIssue = JSONSerializer.Deserialize<Issue>(jsonResponse);
					return Details(createdIssue.Key).Result;
				}
				Error error = JSONSerializer.DeserializeWithDirectory<Error>(jsonResponse);
				StringBuilder errorText = new StringBuilder();
				foreach (var message in error.ErrorMessages) {
					errorText.AppendLine(message);
				}
				if (error.Errors != null) {
					foreach (var key in error.Errors.Keys) {
						errorText.AppendLine(key + " = " + error.Errors[key]);
					}
				}
				Debug.WriteLine("Error: " + errorText.ToString());
				responseTask.Result.EnsureSuccessStatusCode();
				return null;
			});
		}

		/// <summary>
		/// Comment an issue
		/// </summary>
		/// <param name="key"></param>
		/// <param name="comment"></param>
		/// <returns></returns>
		public Task<bool> Comment(string key, Comment comment) {
			var stringContent = new StringContent(JSONSerializer.Serialize(comment), Encoding.UTF8, "application/json");
			return client.PostAsync(string.Format("issue/{0}/comment", key), stringContent).ContinueWith(responseTask => {
				responseTask.Result.EnsureSuccessStatusCode();
				return true;
			});
		}

		/// <summary>
		/// Comment an issue
		/// </summary>
		/// <param name="key"></param>
		/// <param name="comment"></param>
		/// <returns></returns>
		public Task<bool> Comment(string key, string comment) {
			return Comment(key, new Comment {
				Body = comment
			});
		}

		/// <summary>
		/// Attacht the stream as filename to the jira with the supplied key
		/// </summary>
		/// <param name="key"></param>
		/// <param name="filename"></param>
		/// <param name="content">HttpContent like StreamContent or ByteArrayContent</param>
		public Task<bool> Attach(string key, string filename, HttpContent content, CancellationToken token) {
			var requestContent = new MultipartFormDataContent();
			requestContent.Add(content, "file", filename);

			// Make sure the mimetype is specified, this fixes problems with the attachment renderer from JIRA
			content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(filename));

			string attachUrl = string.Format("issue/{0}/attachments", key);
			return client.PostAsync(attachUrl, requestContent, token).ContinueWith((response) => {
				response.Result.EnsureSuccessStatusCode();
				return true;
			});
		}

		public Task<bool> Attach(string key, string filename, IBinaryContainer attachment, CancellationToken token) {
			return Attach(key, filename, new ByteArrayContent(attachment.ToByteArray()), token);
		}

		public Task<bool> Attach(string key, string filename, Stream stream, CancellationToken token) {
			return Attach(key, filename, new StreamContent(stream), token);
		}
		
	}
}
